# [1.5.0](https://gitlab.gwdg.de/fe/classicmayan/rdf-database/compare/v1.4.13...v1.5.0) (2022-12-08)


### Features

* add new gitlab ci workflow, increase spring version ([98034c0](https://gitlab.gwdg.de/fe/classicmayan/rdf-database/commit/98034c07d650f364c4cf28bb01004fbaa2097ec9))
