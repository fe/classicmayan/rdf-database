# ClassicMayan RDF Database

> Database for persons and bibliographic references in the IDIOM project

RDF database is designed for the use with the [TextGrid](https://textgrid.de) RDF Object Editor. It uses [tgFormsMayaImpl](https://gitlab.gwdg.de/fe/classicmayan/tg-forms-classicmayan-impl), a JavaScript library to generate HTML forms from Turtle RDF representations.

## Usage

If you have [node.js](http://nodejs.org), [Bower](http://bower.io) and [Apache Maven](http://maven.apache.org) installed, you can simply run the start script:

```sh
$ ./buildWebapp.sh
```

Now, you can configure the TextGrid RDF Object Editor to use [http://localhost:9090](http://localhost:9090).
