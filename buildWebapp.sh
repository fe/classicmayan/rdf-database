#!/usr/bin/env bash

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")

JSDIR=$DIR/src/js
WEBAPPDIR=$DIR/src/main/webapp

# cleanup
if [[ ! -d $WEBAPPDIR/js/ ]]; then
  mkdir $WEBAPPDIR/js/
fi

# build tgforms
cd $JSDIR/tgformsmayaimpl
cake build # minify

# build datahandler
cd $JSDIR/datahandler
coffee -bc *.coffee

# node modules
cd $JSDIR
npm install

#  n3 browserify (TODO: still necessary?)

if [[ ! -e $JSDIR/node_modules/n3/browser/n3-browser.min.js ]]; then
	echo "no browser version of n3.js, building now... (please hold the line)"
	cd $JSDIR/node_modules/n3

	#npm install which@1.0.9
	npm install
	npm run browser
fi

# copy tgforms
cp $JSDIR/tgformsmayaimpl/build/tgForms.js $WEBAPPDIR/js/tgForms.min.js
cp $JSDIR/tgformsmayaimpl/tgFormsUtilities.js $WEBAPPDIR/js/
cp $JSDIR/tgformsmayaimpl/tgForms.css $WEBAPPDIR/css/
# copy all datahandler js
cp $JSDIR/datahandler/*.js $WEBAPPDIR/js/
# copy npm deps
cp $JSDIR/node_modules/bootstrap/dist/js/bootstrap.min.js $WEBAPPDIR/js/
cp $JSDIR/node_modules/bootstrap/dist/css/bootstrap.min.css $WEBAPPDIR/css/
cp -r $JSDIR/node_modules/bootstrap/dist/fonts $WEBAPPDIR/fonts
cp $JSDIR/node_modules/mustache/mustache.min.js $WEBAPPDIR/js/
cp $JSDIR/node_modules/n3/browser/n3-browser.min.js $WEBAPPDIR/js/
cp $JSDIR/node_modules/jquery/dist/jquery.min.js $WEBAPPDIR/js/
