tgf = new tgForms

caret = ' <span class="caret"></span>'
digilibURL = 'http://textgrid-esx1.gwdg.de/1.0/digilib/rest/digilib/'
currentField = {}
field = {}
tgURI = ""
tgSID = ""
type = ""
changedType = ""
formularName = ""
originFormularType = ""
modalToLoad = ""
queryFolder = "/home/max/textGridModules/bdnDatabase/src/main/webapp/sparql/"
next=false
nextValue = 0
counter = 1
openSavedContent=false
sparqlEndpoint = "https://www.classicmayan.org/trip/metadata/query"

sourceHTML = "<div class=\"form-group tgforms:button dct:isReferencedBy\" data-tgforms-type=\"tgforms:button\" data-tgforms-name=\"dct:isReferencedBy\">\r\n <span class=\"label\">Source<\/span><span class=\"resource glyphicon glyphicon-link icon-link\" aria-hidden=\"true\"><\/span><span class=\"repeat glyphicon glyphicon-plus icon-plus\" aria-hidden=\"true\"><\/span><span class=\"delete glyphicon glyphicon-minus icon-minus\" aria-hidden=\"true\"><\/span>\r\n <span title=\"Reference for the described object in form of a bibliographic citation.\" class=\"glyphicon glyphicon-info-sign icon-info\"><\/span>\r\n <div>\r\n <span class=\"value\">dct:isReferencedByID_TO_REPLACE<\/span>\r\n \r\n <button type=\"button\" class=\"btn btn-default formularOp\" id=\"source\" disabled=\"disabled\">Choose<\/button>\r\n <button type=\"button\" class=\"btn btn-default source\" id=\"removeFormular\">Remove<\/button>\r\n \r\n <button type=\"button\" data-toggle=\"collapse\" data-target=\"div#dct_isReferencedByID_TO_REPLACE\"><span class=\"glyphicon glyphicon-menu-down\"><\/span><\/button><div id=\"dct_isReferencedByDCTID_TO_REPLACE\" class=\"isReferencedBy subformular collapse in \" source\"=\"\"> <fieldset> <legend> <span title=\"A Source is a statement in form of a bibliographic citation, which is an evidence for an assertion about the described object.\">Source<\/span> <\/legend><div class=\"form-group tgforms:button idiom:zoteroURI\" data-tgforms-type=\"tgforms:button\" data-tgforms-name=\"idiom:zoteroURI\">\r\n <span class=\"label\">Zotero URI<\/span>\r\n <div>\r\n <span class=\"value\">ZOTERO_ID<\/span>\r\n \r\n <button type=\"button\" class=\"btn btn-default searchOp\">Choose<\/button>\r\n <button type=\"button\" class=\"btn btn-default\" id=\"removeContent\" disabled=\"disabled\">Remove<\/button>\r\n<button type=\"button\" class=\"btn btn-default\" id=\"reloadCitation\">Reload<\/button>\r\n \r\n <\/div>\r\n <\/div><div class=\"form-group tgforms:text dct:bibliographicCitation\" data-tgforms-type=\"tgforms:text\" data-tgforms-name=\"dct:bibliographicCitation\">\r\n <label>\r\n <span class=\"label\">Bibliographic Citation<\/span>\r\n \r\n \r\n \r\n \r\n <input id=\"dct:bibliographicCitation\" type=\"text\" class=\"form-control\" readonly=\"readonly\">\r\n \r\n <\/label>\r\n <span>\r\n <\/span><\/div><div class=\"form-group tgforms:text schema:pagination\" data-tgforms-type=\"tgforms:text\" data-tgforms-name=\"schema:pagination\">\r\n <label>\r\n <span class=\"label\">Page<\/span>\r\n \r\n \r\n \r\n \r\n <input id=\"schema:pagination\" type=\"text\" class=\"form-control\">\r\n \r\n <\/label>\r\n <span>\r\n <\/span><\/div><div class=\"form-group tgforms:dropdown idiom:attributionType\" data-tgforms-type=\"tgforms:dropdown\" data-tgforms-name=\"idiom:attributionType\">\r\n <span class=\"label\">Attribution Type<\/span>\r\n <span title=\"Please select a term from the list. Author (explicit) indicates knowledge explicitly attributed to the author. Author (implicitly) indicates knowledge implicitly attributed to the author. External source (implicit) indicates knowledge attributed to a named external source (either explicitly or as a reference). External source (explicit) indicates knowledge attributed to a nameless external source.\" class=\"glyphicon glyphicon-info-sign icon-info\"><\/span>\r\n <div class=\"btn-group\">\r\n \r\n <button id=\"idiom:attributionType\" type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">\r\n <span class=\"value\">author (explicit)<\/span><span class=\"caret\"><\/span>\r\n <\/button>\r\n \r\n <ul class=\"dropdown-menu dropdown-menu\" role=\"menu\">\r\n <li><a href=\"javascript:return false\">external source (explicit)<\/a><\/li>\r\n <li><a href=\"javascript:return false\">external source (implicit)<\/a><\/li>\r\n <li><a href=\"javascript:return false\">author (implicit)<\/a><\/li>\r\n <li><a href=\"javascript:return false\">author (explicit)<\/a><\/li>\r\n <\/ul>\r\n <\/div>\r\n <\/div>\r\n<div class=\"form-group tgforms:dropdown idiom:hasConfidenceLevel\" data-tgforms-type=\"tgforms:dropdown\" data-tgforms-name=\"idiom:hasConfidenceLevel\">\r\n <span class=\"label\">Confidence Level<\/span><span class=\"mandatory glyphicon glyphicon-asterisk icon-asterisk\" aria-hidden=\"true\"><\/span>\r\n <span title=\"Please select a term from the list. Hypothetical knowledge: low certainty. Dubitative knowledge: higher likelihood but short of complete certainty. Doxastic knowledge: complete certainty, reflecting an accepted, known and\/or proven fact.\" class=\"glyphicon glyphicon-info-sign icon-info\"><\/span>\r\n <div class=\"btn-group\">\r\n \r\n <button id=\"idiom:hasConfidenceLevel\" type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" required=\"\">\r\n <span class=\"value\">doxastic knowledge<\/span><span class=\"caret\"><\/span>\r\n <\/button>\r\n \r\n <ul class=\"dropdown-menu dropdown-menu\" role=\"menu\">\r\n <li><a href=\"javascript:return false\">lack of knowledge<\/a><\/li>\r\n <li><a href=\"javascript:return false\">dubitative knowledge<\/a><\/li>\r\n <li><a href=\"javascript:return false\">hypothetical knowledge<\/a><\/li>\r\n <li><a href=\"javascript:return false\">doxastic knowledge<\/a><\/li>\r\n <\/ul>\r\n <\/div>\r\n <\/div>\r\n<\/fieldset><\/div><\/div>\r\n <\/div>"

###########################
## Object Type Selection ##
###########################

$(document).on("ready click", "li.objectTypeSelection", (e) ->
  e.preventDefault()
  formularType = $(this).attr('id').replace(/_/, ':')
  originFormularType = formularType;
  generateForm(formularType)
)

generateForm = (formularType) ->
  changedType = formularType.split ":"
  changedType = changedType[1]
  originFormularType = formularType;
  $.ajax(
    type: "GET",
    url: "idiom.ttl",
    mimeType: "text/turtle"
  )
  .done((data) ->
    addCall = ->
      if(changedType == "Artefact" && openSavedContent == false)
        $.when(tgf.buildForm(formularType, "div#content")
        ).then(checkIdiomNumber())
      else
        tgf.buildForm(formularType, "div#content")
    tgf.addTurtle(data, addCall)
  )


##############################
## Button Load Search Modal ##
##############################

$(document).on("ready click", "button.searchOp", (e) ->
  e.preventDefault()
  $this = $(this)
  $(this).attr('disabled', 'disabled')
  $(this).next().removeAttr('disabled')
  load = $(this).parent().parent().attr('data-tgforms-name').replace(/:/, '_')
  currentField = $this.closest "div.form-group"

  modalToLoad = load
  $('h3.resultLength').remove()
  $('button#prev').attr('style', 'display:none')
  $('button#next').attr('style', 'display:none')
  console.log "MODAL_TO_LOAD: " + modalToLoad
  $('div#' + load).modal 'show'
  $('div#' + load + '-results').empty()
  $('div#' + load + '-input').text('')

  if modalToLoad == "crm_P42_assigned" || modalToLoad == "idiom_orientationOfArtefact" || modalToLoad == "idiom_conditionType" || modalToLoad == "idiom_actorNameType" || modalToLoad == "idiom_assignedMaterial" || modalToLoad == "idiom_P14_1_in_the_role_of" || modalToLoad == "idiom_groupType" || modalToLoad == "idiom_placeType" || modalToLoad == "idiom_assignedTechnique" || modalToLoad == "idiom_assignedShapeType" || modalToLoad == "idiom_activityType" || modalToLoad == "idiom_profession" || modalToLoad == "idiomcat_hasIcon"
      handleVocabulary(modalToLoad)

  $('button#' + modalToLoad + 'Search').unbind('click')
  $('button#' + modalToLoad + 'Search').click((e) ->
  #$(document).on("ready click", 'button#' + modalToLoad + 'Search', (e) ->
    e.preventDefault()
    $this = $(this)
    $this.prop('disabled', true)
    $this.text('Please Wait')
    $('div#' + load + '-results').empty()
    $('h3.resultLength').remove()

    if modalToLoad == "idiom_relationshipChild" ||
       modalToLoad == "idiom_relationshipGrandchild" ||
       modalToLoad == "idiom_relationshipSibling" ||
       modalToLoad == "idiom_relationshipSpouse" ||
       modalToLoad == "idiom_relationshipRelation" ||
       modalToLoad == "idiom_relationshipDivineTutelary" ||
       modalToLoad == "idiom_relationshipDescendant" ||
       modalToLoad == "idiom_commissioner"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getEpirgaphicActorQuery(keyword, modalToLoad)
         url= sparqlEndpoint

    if modalToLoad == "crm_P62_depicts"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getMiscQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "crm_P46_is_composed_of"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getArtefactQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "idiom_producer" ||
       modalToLoad == "crm_P17_was_motivated_by"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getEpigraphicQuery(keyword)
         url= sparqlEndpoint

    if modalToLoad == "idiom_hasActor" || modalToLoad == "idiom_isSameAsActor"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getAllActorQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "idiom_custodian" ||
       modalToLoad == "crm_P22_transferred_title_to" ||
       modalToLoad == "idiom_explorer"||
       modalToLoad == "crm_P23_transferred_title_from" ||
       modalToLoad == "idiom_assignedBy" ||
       modalToLoad == "dc_source"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getNonEpigraphicQuery(keyword)
         url= sparqlEndpoint

    if modalToLoad == "crm_P89_falls_within" ||
       modalToLoad == "crm_P121_overlaps_with" ||
       modalToLoad == "crm_P122_borders_with" ||
       modalToLoad == "crm_P74_has_current_or_former_residence" ||
       modalToLoad == "idiom_findingSpotOfRemains" ||
       modalToLoad == "crm_P7_took_place_at"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getPlaceQuery(keyword)
         url= sparqlEndpoint

    if modalToLoad == "idiom_wasFoundAt"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getDiscoveryQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "crm_P12i_was_present_at" ||
       modalToLoad == "crm_P16i_was_used_for" ||
       modalToLoad == "idiom_namingRelatedActivity"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getActivityQuery(keyword)
         url= sparqlEndpoint

    if modalToLoad == "idiom_formerCustody" ||
       modalToLoad == "idiom_currentCustody"
         keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
         query = getCustodyQuery(keyword)
         url= sparqlEndpoint
         console.log query

    if modalToLoad == "crm_P24i_changed_ownership_through"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getAcquisitionQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "crm_P108i_was_produced_by"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getProductionQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "crm_P107i_is_former_or_current_member_of"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getGroupQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "idiom_relatedActivity"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getAllActivitiesQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "crm_P113i_was_removed_by"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getPartRemovalQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "idiom_isShownBy"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getVisualDocumentQuery(keyword)
      url= sparqlEndpoint

    if modalToLoad == "idiom_wasDedicatedBy"
      keyword = keywordReplacing($('input#' + modalToLoad + '-input').val())
      query = getDedicationQuery(keyword)
      url = sparqlEndpoint


    ## SignCatalog##

    if modalToLoad == "idiomcat_isMergedInto" ||
       modalToLoad == "idiomcat_containsIconicElementsFrom" ||
       modalToLoad == "idiomcat_sharesDiagnosticElementsWith" ||
       modalToLoad == "idiomcat_isDividedInto" ||
       modalToLoad == "idiomcat_isDerivedFrom"
         keyword = $('input#' + modalToLoad + '-input').val()
         query = getGraphQuery(keyword)
         url= sparqlEndpoint

    if modalToLoad == "idiomcat_isDistinctAllographTo" ||
       modalToLoad == "idiomcat_isGraphOf" ||
       modalToLoad == "idiomcat_isSameAs"
         keyword = $('input#' + modalToLoad + '-input').val()
         if isFinite(keyword)
           keyword = $('input#' + modalToLoad + '-input').val()
           query = getSignQuery(keyword)
           url= sparqlEndpoint
         else
           keyword = $('input#' + modalToLoad + '-input').val()
           query = getSignQueryForTransliterationValue(keyword)
           url= sparqlEndpoint
    if(modalToLoad == "crm_P128_carries")
      processTEIQuery(modalToLoad)

    if(modalToLoad == "idiom_shows")
      processConedaKorQuery(modalToLoad,1)

    if modalToLoad == "idiom_gndID"
      pndSearch(modalToLoad)

    if modalToLoad == "idiom_geonamesID"
      geonamesSearch(modalToLoad)

    if modalToLoad == "idiom_tgnID"
      gettySearch2(modalToLoad)

    if modalToLoad == "idiom_zoteroURI"
      zoteroAPI2(modalToLoad,
                 "https://api.zotero.org/users/1757564/" +
                 "items?sort=date&limit=40&format=json" +
                 "&key=dayzvtbgcHajqLEqkB0WAj72&q=",
                 keyword)

    if modalToLoad == "crm_P46_is_composed_of"
      query = getArtefactQuery(keyword)
      url= sparqlEndpoint

    request = {
      'query' : queryPrefixes + query
    }

    $.ajax(
      type: 'GET',
      url: url,
      cache: false,
      dataType: "json",
      data: request
    ).done((data) ->
      results = data['results']['bindings']
      if modalToLoad == "idiom_relationshipChild" ||
         modalToLoad == "idiom_relationshipGrandchild" ||
         modalToLoad == "idiom_relationshipSibling" ||
         modalToLoad == "idiom_relationshipSpouse" ||
         modalToLoad == "idiom_relationshipRelation" ||
         modalToLoad == "idiom_relationshipDivineTutelary" ||
         modalToLoad == "idiom_relationshipDescendant" ||
         modalToLoad == "idiom_commissioner"
           appendEpigraphicActor(results, modalToLoad)
      if modalToLoad == "crm_P17_was_motivated_by"
        appendEpigraphic(results, modalToLoad)
      if modalToLoad == "crm_P62_depicts"
        appendMisc(results, modalToLoad)
      if modalToLoad == "crm_P46_is_composed_of"
        appendArtefact(results, modalToLoad)
      if modalToLoad == "idiom_producer"
        appendEpigraphic(results, modalToLoad)
      if modalToLoad == "idiom_hasActor" || modalToLoad == "idiom_isSameAsActor"
        appendAllActor(results, modalToLoad)
      if modalToLoad == "idiom_custodian" ||
         modalToLoad == "crm_P22_transferred_title_to" ||
         modalToLoad == "crm_P23_transferred_title_from" ||
         modalToLoad == "idiom_explorer" ||
         modalToLoad == "idiom_assignedBy" ||
         modalToLoad == "dc_source"
           appendNonEpigraphic(results, modalToLoad)
      if modalToLoad == "crm_P89_falls_within" ||
         modalToLoad == "crm_P121_overlaps_with" ||
         modalToLoad == "crm_P122_borders_with" ||
         modalToLoad == "crm_P74_has_current_or_former_residence" ||
         modalToLoad == "idiom_findingSpotOfRemains" ||
         modalToLoad == "crm_P7_took_place_at"
           appendPlace(results, modalToLoad)
      if modalToLoad == "idiom_wasFoundAt"
        appendDiscovery(results, modalToLoad)
      if modalToLoad == "crm_P12i_was_present_at" ||
         modalToLoad == "crm_P16i_was_used_for" ||
         modalToLoad == "idiom_namingRelatedActivity"
           appendActivity(results, modalToLoad)
      if modalToLoad == "idiom_formerCustody" ||
         modalToLoad == "idiom_currentCustody"
           appendCustody(results, modalToLoad)
      if modalToLoad == "crm_P24i_changed_ownership_through"
        appendAcquisition(results, modalToLoad)
      if modalToLoad == "crm_P108i_was_produced_by"
        appendProduction(results, modalToLoad)
      if modalToLoad == "crm_P107i_is_former_or_current_member_of"
        appendGroup(results, modalToLoad)
      if modalToLoad == "idiom_relatedActivity"
        appendAllActivities(results, modalToLoad)
      if modalToLoad == "crm_P113i_was_removed_by"
        appendPartRemoval(results, modalToLoad)
      if modalToLoad == "idiom_isShownBy"
        appendVisualDocument(results, modalToLoad)
      if modalToLoad == "idiom_wasDedicatedBy"
        appendDedication(results, modalToLoad)
      if modalToLoad == "idiomcat_isDistinctAllographTo" ||
         modalToLoad == "idiomcat_isSameAs" ||
         modalToLoad == "idiomcat_isGraphOf"
        appendSign(results, modalToLoad)
      if modalToLoad == "idiomcat_isMergedInto" ||
         modalToLoad == "idiomcat_containsIconicElementsFrom" ||
         modalToLoad == "idiomcat_sharesDiagnosticElementsWith" ||
         modalToLoad == "idiomcat_isDividedInto" ||
         modalToLoad == "idiomcat_isDerivedFrom"
          appendGraph(results, modalToLoad)

    )
  )
)


setTheId = (modalToLoad, id) ->
  id = $('div#' + modalToLoad +
          '-results input[name="' + modalToLoad + '-radios"]:checked').val()
  if id
    currentField.find('span.value + span').remove()
    currentField.find('span.value').text(id)
  $('div#' + modalToLoad + '-results').empty()
  $('div#' + modalToLoad + 'Search').modal 'show'


setMultipleIDs = (modalToLoad, id, labelText) ->

  i=0;

  $('div#'+modalToLoad).find('input[type="checkbox"]:checked').each ->
    console.log "tada"
    if(i>0)
      if(doubletteCheck($(this).val()))
         console.log "i is: " + i
         if(modalToLoad == "idiom_zoteroURI")
           #console.log($(this));
           setTimeout(setMultipleIDs, 1000)
           dividforreplace = generateId()
           #console.log dividforreplace
           #$(this).val() console.log sourceHTML
           sourceHTML = sourceHTML.replace("ZOTERO_ID", $(this).val())
           $("div.dct\\:isReferencedBy").last().after(sourceHTML.replace("ID_TO_REPLACE",dividforreplace))
           console.log "ZOTERO_ID:"
         console.log $(this).val()
           
         $("div." + modalToLoad.replace("_", "\\:")).last().after("<div class=\"form-group tgforms:button " + modalToLoad.replace("_", "\:") + "\" data-tgforms-type=\"tgforms:button\" data-tgforms-name=\"" + modalToLoad.replace("_", "\:") + "\"><span class=\"label\">" + labelText + "</span><span class=\"repeat glyphicon glyphicon-plus icon-plus\" aria-hidden=true\"><span class=\"delete glyphicon glyphicon-minus icon-minus\" aria-hidden=\"true\"></span></span><span title=\"Choose one ore more images of the described artefact from the Maya Image Database (project distribution of ConedaKor).\" class=\"glyphicon glyphicon-info-sign icon-info\"></span><div><span class=\"value\">" + $(this).val() + "</span><button type=\"button\" class=\"btn btn-default searchOp\" disabled=\"disabled\">Choose</button><button type=\"button\" class=\"btn btn-default\" id=\"removeContent\">Remove</button></div></div>");
         #$("div.idiom\\:shows").last().after("<div class=\"form-group tgforms:button idiom:shows\" data-tgforms-type=\"tgforms:button\" data-tgforms-name=\"idiom:shows\"><span class=\"label\">Image in Maya Image Database</span><span class=\"repeat glyphicon glyphicon-plus icon-plus\" aria-hidden=true\"><span class=\"delete glyphicon glyphicon-minus icon-minus\" aria-hidden=\"true\"></span></span><span title=\"Choose one ore more images of the described artefact from the Maya Image Database (project distribution of ConedaKor).\" class=\"glyphicon glyphicon-info-sign icon-info\"></span><div><span class=\"value\">" + $(this).val() + "</span><button type=\"button\" class=\"btn btn-default searchOp\" disabled=\"disabled\">Choose</button><button type=\"button\" class=\"btn btn-default\" id=\"removeContent\">Remove</button></div></div>")
      else
        alert $(this).val() + " ID is already choosen and won't be entered"
    else
      console.log "TADA"
      if (i==0 && doubletteCheck($(this).val()))
        console.log(currentField)
        currentField.find('span.value + span').remove()
        currentField.find('span.value').text($(this).val())
      else
        alert $(this).val() + " ID is already choosen and won't be entered"
      $('div#' + modalToLoad + '-results').empty()
      $('div#' + modalToLoad + 'Search').modal 'show'
    i++

setMultipleZoteroIDs = (modalToLoad, id, refID) ->
    console.log "IDIOM"
    i=0;
    $('div#'+modalToLoad).find('input[type="checkbox"]:checked').each ->
      if(i>0)
        setTimeout(setMultipleIDs, 1000)
        dividforreplace = generateId()
        console.log "ZOT URI: "  + $(this).val()
        sourceHTMLtmp = sourceHTML
        sourceHTMLtmp = sourceHTMLtmp.replace("ZOTERO_ID", $(this).val())
        sourceHTMLtmp = sourceHTMLtmp.replace("DCTID_TO_REPLACE", dividforreplace)
        $("div.dct\\:isReferencedBy").last().after(sourceHTMLtmp.replace("ID_TO_REPLACE",dividforreplace))
        #console.log  $(this).parent().parent().attr('id')
        #console.log dividforreplace.replace("\\:", "_")
        bibCit = $(this).next().text()
        console.log bibCit.substring(bibCit.indexOf("]") + 4, bibCit.length)
        bibCit = bibCit.substring(bibCit.indexOf("]") + 4, bibCit.length)
        #bibCit = bibCit.substring((bibCit.search /] /) + 4, bibCit.length)
        console.log 'div#dct_isReferencedBy' + dividforreplace +  ' div.dct\\:bibliographicCitation label input'
        console.log $('div#dct_isReferencedBy' + dividforreplace +  ' div.dct\\:bibliographicCitation label input')
        console.log bibCit
        $('div#dct_isReferencedBy' + dividforreplace +  ' div.dct\\:bibliographicCitation label input').val(bibCit)
        #$("div#" + modalToLoad + " input#dct\\:bibliographicCitation").val(bibCit)
      else
        console.log "TADA"
        if (i==0 )
          currentField.find('span.value + span').remove()
          currentField.find('span.value').text($(this).val())
          bibCit = $(this).next().text()
          bibCit = bibCit.substring(bibCit.indexOf("]") + 4, bibCit.length)
          console.log currentField
          console.log(currentField.parent().parent().attr('id'))
          refId = currentField.parent().parent().attr('id')
          console.log("BBBBBBBBBBBBBBB: " + bibCit)
          console.log("REEEEEEEEEEEEEEF: " + refId)
          currentField.find('div.dct\\:bibliographicCitation label input').val(bibCit)
          $('div#' + refId +  ' div.dct\\:bibliographicCitation label input').val(bibCit)
        else
          alert $(this).val() + " ID is already choosen and won't be entered"
        $('div#' + modalToLoad + '-results').empty()
        $('div#' + modalToLoad + 'Search').modal 'show'
      i++


doubletteCheck = (id) ->
    alreadyChosenIDs = []
    $("div.idiom\\:shows span.value").each ->
      alreadyChosenIDs.push($(this).text())

    for ids in alreadyChosenIDs
      console.log id + " == " + ids
      if(id == ids)
        console.log "CHOOSEN"
        #alert ids + " ID is already choosen and won't be entered"
        return false
    return true


pndSearch = (modalToLoad) ->
  keyword = encodeURIComponent($('input#' + modalToLoad + '-input').val())
  namekey = "preferred_name"
  infokey = "info"
  resultkey = "person"
  searchurl = "https://ref.dariah.eu/beta/pndsearch/pndquery.xql?ln="
  #searchurl = "https://classicmayan.org/gnd/beta/pndsearch/pndquery.xql?ln="
  idprefix = ""

  if keyword.indexOf("pnd") == 0
    namekey="preferredNameForThePerson"
    infokey="biographicalInformation"
    resultkey="entry"
    searchurl = "https://ref.dariah.eu/beta/pndsearch/pndquery.xql?id="
    idprefix = "pnd:"
    keyword = keyword.substring(6, keyword.length)

  $.ajax(
    type: 'GET',
    url: searchurl + keyword
    cache: false
  ).done((data) ->
    results = $(data).find(resultkey)
    $(results).each((key, value) ->
      id = $(value).attr('id')
      name = $(value).find(namekey).text()
      info = $(value).find(infokey).text()

      if $('div#' + modalToLoad + ' input[value="' + id + '"]').length
        return true

      $('div#' + modalToLoad + '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + idprefix + id + '">
                <strong>' +  name + ':</strong> ' + info + '
            </label>
          </div>'
      )
    )
    $('button#set-' + modalToLoad + 'Ext').click((e) ->
      e.preventDefault()
      id = $('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()
      if id
        currentField.find('span.value + span').remove()
        currentField.find('span.value').text(id)
    )
  )

geonamesSearch = (modalToLoad) ->
  keyword = $('input#' + modalToLoad + '-input').val()
  searchurl = "https://classicmayan.org/geonames/search?username=idiomtest&maxRows=1000&q="
  #searchurl = "http://api.geonames.org/search?username=idiomtest&maxRows=1000&q="
  resultkey="geoname"

  $.ajax(
    type: 'GET',
    url: searchurl + keyword
    cache: false
  ).done((data) ->

    results = $(data).find(resultkey)
    $(results).each((key, value) ->

      geonameId = $(value).find('geonameId').text()
      name = $(value).find("name").text()
      toponymName = $(value).find("toponymName").text()
      countryCode = $(value).find("countryCode").text()
      countryName = $(value).find("countryName").text()
      lat = $(value).find("lat").text()
      lng = $(value).find("lng").text()
      fcode = $(value).find("fcode").text()

      if $('div#' + modalToLoad + ' input[value="' + geonameId + '"]').length
        return true

      $('div#' + modalToLoad + '-results').append(
        '<div class="radio">
          <label>
            <input type="radio" name="' + modalToLoad + '-radios" value="' + geonameId + '">
              <strong>' + name + ':</strong> ' + toponymName + ", " +  countryCode + ", " + countryName + ", " + lat + ", " + lng + ", " + fcode + '</label>
         </div>'
      )
    )

    $('button#set-' + modalToLoad).click((e) ->
      e.preventDefault()
      id = $('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()
      if id
        currentField.find('span.value + span').remove()
        currentField.find('span.value').text(id)
    )
  )

zoteroAPI2 = (modalToLoad, url, counter) ->

  $('div#' + modalToLoad + '-results').empty()
  $('div#' + modalToLoad + '-results').append('<img id="ajaxloader" src="ajaxLoader.gif" height="42" width="42"/>')
  $("h1#emptyListHeader").remove()
  keyword = $('input#' + modalToLoad + '-input').val()
  searchurl = url + keyword

  if next==false
    counter = 1

  $.ajax(
    type: 'GET',
    url: searchurl,
    cache: false,
    dataType:'json'
  ).done((data) ->

    if data.length > 0
      $('button#prev').removeAttr('style')
      $('button#next').removeAttr('style')
      counterDataSet = 0
      for dates in data
        id = "https://www.zotero.org/idiom_bibliography/items/" + data[counterDataSet]['data']['key']
        authors = data[counterDataSet]['data']['creators']
        title = data[counterDataSet]['data']['title'] + ". "
        if data[counterDataSet]['data']['date'].length == 0
          date = ""
        else
          date = " (" + data[counterDataSet]['data']['date'] + "): "
        itemType = data[counterDataSet]['data']['itemType']

        authorsDisp = []
        editorsDisp = []
        authorCount = 0

        for author in authors

          if authorCount == 0
            if author.creatorType == "author" || author.creatorType == "cartographer" || author.creatorType == "contributor" || author.creatorType == "presenter"
              authorsDisp.push " " + author.lastName + ", " + author.firstName
            if author.creatorType == "editor"
              if authorsDisp.length == 0
                editorsDisp.push " " + author.lastName + ", " + author.firstName

          else
              if author.creatorType == "author"
                authorsDisp.push " " + author.firstName + " " + author.lastName
              if author.creatorType == "editor"
                editorsDisp.push " " + author.firstName + " " + author.lastName
          if author.creatorType == "author"
            authorCount++


        if itemType == "journalArticle"
          if data[counterDataSet]['data']['volume'].length == 0
            volume = ""
          else
            volume = data[counterDataSet]['data']['volume'] + ": "

          if data[counterDataSet]['data']['publicationTitle'].length == 0
            publicationTitle = ""
          else
            publicationTitle = data[counterDataSet]['data']['publicationTitle'] + " "

          if data[counterDataSet]['data']['pages'].length == 0
            pages = ""
          else
            pages = data[counterDataSet]['data']['pages'] + ". "

          citation = authorsDisp + date + title + publicationTitle + volume + pages

        if itemType == "blogPost" || itemType == "encyclopediaArticle" || itemType == "webpage"
          if data[counterDataSet]['data']['url'].length == 0
            url = ""
          else
            url = data[counterDataSet]['data']['url']

          if itemType == "blogPost"

            if data[counterDataSet]['data']['blogTitle'].length == 0
              blogTitle = ""
            else
              blogTitle = data[counterDataSet]['data']['blogTitle'] + ". "

          if data[counterDataSet]['data']['dateAdded'].length == 0
            accessed = ""
          else
            accessed = " [" + data[counterDataSet]['data']['dateAdded'] + "]"
          citation = authorsDisp + date +  title + "Electronic Document. " + url + accessed

        if itemType == "book"
          if data[counterDataSet]['data']['publisher'].length == 0
            publisher == ""
          else
            publisher = data[counterDataSet]['data']['publisher'] + ", "

          if data[counterDataSet]['data']['place'].length == 0
            publishingPlace = ""
          else
            publishingPlace = data[counterDataSet]['data']['place'] + "."

          if data[counterDataSet]['data']['seriesNumber'].length == 0
            seriesNumber = ""
          else
            seriesNumber = " " + data[counterDataSet]['data']['seriesNumber'] + ". "
          if seriesNumber.length == 3
            seriesNumber = ""
          if data[counterDataSet]['data']['series'].length == 0
            series = ""
          else
            series = data[counterDataSet]['data']['series']

          if data[counterDataSet]['data']['volume'].length == 0
            volume = ""
          else
            volume = "(" + data[counterDataSet]['data']['volume'] + ")."

          if authorsDisp.length == 0
            authorsDisp = editorsDisp
          citation = authorsDisp + date + title + series + seriesNumber + volume + publisher + publishingPlace

        if itemType == "bookSection"
          if data[counterDataSet]['data']['volume'].length == 0
            volume = ""
          else
            volume = ". Vol. "  + data[counterDataSet]['data']['volume']
          if data[counterDataSet]['data']['series'].length == 0
            series = ""
          else
            series = data[counterDataSet]['data']['series']
          if data[counterDataSet]['data']['seriesNumber'].length == 0
            seriesNumber = ""
          else
            seriesNumber = " " + data[counterDataSet]['data']['seriesNumber'] + ". "
          if data[counterDataSet]['data']['bookTitle'].length == 0
            bookTitle = ""
          else
            bookTitle = " In: " + data[counterDataSet]['data']['bookTitle']
          if data[counterDataSet]['data']['pages'].length == 0
            pages = ""
          else
            pages = ", pp. " + data[counterDataSet]['data']['pages'] + ". "
          if typeof publisher == "undefined"
            publisher = ""
          else
            publisher = data[counterDataSet]['data']['publisher'] + ", "
          if data[counterDataSet]['data']['place'].length == 0
            publishingPlace = ""
          else
            publishingPlace = data[counterDataSet]['data']['place'] + "."

          if authorsDisp.length == 0
            authorsDisp = editorsDisp
          citation = authorsDisp + date + title + bookTitle + volume + ", edited by: " + editorsDisp + pages + series + seriesNumber + publisher + publishingPlace

        if itemType == "manuscript"
          if data[counterDataSet]['data']['manuscriptType'].length == 0
            manuscriptType = ""
          else
            manuscriptType = data[counterDataSet]['data']['manuscriptType'] + ". "
          if data[counterDataSet]['data']['callNumber'].length == 0
            callNumber = ""
          else
            callNumber = data[counterDataSet]['data']['callNumber'] + "."
          if data[counterDataSet]['data']['archive'].length == 0
            archive = ""
          else
            archive = data[counterDataSet]['data']['archive'] + ". "
          citation = authorsDisp + date + title + manuscriptType + archive + callNumber

        if itemType == "thesis"
          if data[counterDataSet]['data']['thesisType'].length == 0
            thesisType = ""
          else
            thesisType = data[counterDataSet]['data']['thesisType'] + ", "
          if data[counterDataSet]['data']['place'].length == 0
            university = ""
          else
            university = data[counterDataSet]['data']['place'] + ". "
          if data[counterDataSet]['data']['university'].length == 0
            place = ""
          else
            place = data[counterDataSet]['data']['university'] + ", "
          if data[counterDataSet]['data']['url'].length == 0
            url = ""
          else
            url = data[counterDataSet]['data']['url']
          citation = authorsDisp + date + title + thesisType + place + university + url

        if itemType == "magazineArticle"
          if data[counterDataSet]['data']['publicationTitle'].length == 0
            publicationTitle = ""
          else
            publicationTitle = data[counterDataSet]['data']['publicationTitle'] + " "
          if data[counterDataSet]['data']['volume'].length == 0
            volume = ""
          else
            volume = data[counterDataSet]['data']['volume'] + ": "
          if data[counterDataSet]['data']['pages'].length == 0
            pages = ""
          else
            pages = data[counterDataSet]['pages']['pages'] + "."

          citation = authorsDisp + date + title + publicationTitle + volume + pages

        if itemType == "newspaperArticle"
          if data[counterDataSet]['data']['publicationTitle'].length == 0
            publicationTitle = ""
          else
            publicationTitle = data[counterDataSet]['data']['publicationTitle'] + " "
          if data[counterDataSet]['data']['section'].length == 0
            section = ""
          else
            section = data[counterDataSet]['data']['section'] + ": "
          if data[counterDataSet]['data']['pages'].length == 0
            pages = ""
          else
            pages = data[counterDataSet]['data']['pages'] + "."

          citation = authorsDisp + date + title + publicationTitle + section + pages

        if itemType == "report"
          if data[counterDataSet]['data']['reportType'].length == 0
            reportType = ""
          else
            reportType = data[counterDataSet]['data']['reportType'] + ". "
          if data[counterDataSet]['data']['institution'].length == 0
            institution = ""
          else
            institution = data[counterDataSet]['data']['institution'] + ", "
          if data[counterDataSet]['data']['place'].length == 0
            place = ""
          else
            place = data[counterDataSet]['data']['place'] + ". "
          if data[counterDataSet]['data']['url'].length == 0
            url = ""
          else
            url = data[counterDataSet]['data']['url']
            if url.length > 0
              dateAdded = data[counterDataSet]['data']['dateAdded']
              url = " " + url + ". [" + data[counterDataSet]['data']['dateAdded'] + "]"

          citation = authorsDisp + date + title + reportType + institution + place  + url

        if itemType == "document"
          if data[counterDataSet]['data']['url'].length == 0
            url = ""
          else
            url = data[counterDataSet]['data']['url']
          if url.length > 0
            dateAdded = data[counterDataSet]['data']['dateAdded']
            url = " " + url + ". [" + data[counterDataSet]['data']['dateAdded'] + "]"
          citation = authorsDisp + date + title + url

        if itemType == "map"
          if data[counterDataSet]['data']['place'].length == 0
            place = ""
          else
            place = data[counterDataSet]['data']['place'] + ". "
          if data[counterDataSet]['data']['url'].length == 0
            url = ""
          else
            url = data[counterDataSet]['data']['url']
            if url.length > 0
              dateAdded = data[counterDataSet]['data']['dateAdded']
              url = " " + url + ". [" + data[counterDataSet]['data']['dateAdded'] + "]"
          if data[counterDataSet]['data']['place'].length == 0
           publisher = ""
          else
            publisher = data[counterDataSet]['data']['publisher'] + ", "

          citation = authorsDisp + ": " + title + publisher + place + url

        if itemType == "presentation"
          if data[counterDataSet]['data']['meetingName'].length == 0
            meetingName = ""
          else
            meetingName = data[counterDataSet]['data']['meetingName']

          citation = authorsDisp + date + title + meetingName

        if $('div#' + modalToLoad + ' input[value="' + id + '"]').length
          return true

        $('img#ajaxloader').remove()
        $('div#' + modalToLoad + '-results').append(
          '<div class="radio">
            <label>
              <input type="checkbox" name="' + modalToLoad + '-radios" value="' + id + '">
                <div class="bibcit"><strong>[' + counter + ']</strong>  ' + citation + ' </div></label>
           </div>'
        )# close appendResults
        counterDataSet++
        counter++
        $("button#idiom_zoteroURISearch").prop('disabled', false)
        $("button#idiom_zoteroURISearch").text('Search')

      $('button#next').unbind("click")
      $('button#next').click((e) ->
        e.preventDefault()
        next=true
        nextValue = nextValue + 40
        zoteroAPI2(modalToLoad, "https://api.zotero.org/users/1757564/items?sort=date&limit=40&start=" + nextValue + "&format=json&key=dayzvtbgcHajqLEqkB0WAj72&q=", counter)
      ) # close button#next
      $('button#prev').unbind("click")
      $('button#prev').click((e) ->
        e.preventDefault()
        next=true
        nextValue = nextValue - 40
        counter = counter - 80
        zoteroAPI2(modalToLoad, "https://api.zotero.org/users/1757564/items?sort=date&limit=40&start=" + nextValue + "&format=json&key=dayzvtbgcHajqLEqkB0WAj72&q=", counter)
      ) # close button#prev

    else
      $("button#idiom_zoteroURISearch").prop('disabled', false)
      $("button#idiom_zoteroURISearch").text('Search')
      $('img#ajaxloader').remove()
      $('div.modal-header').append('<h1 id="emptyListHeader">No Results</h1>')

    $("div#idiom_zoteroURI button.close").click((e) ->
      $("h1#emptyListHeader").remove()
      counter = 1
      nextPage = ""
    ) # close click button.close

    $('button#set-' + modalToLoad + 'Ext').unbind("click")
    $('button#set-' + modalToLoad + 'Ext').click((e) ->
      e.preventDefault()
      $this = currentField
      refId = currentField.parent().parent().parent().parent().parent().parent().attr('id')
      console.log("REFID: " + refId)
      setMultipleZoteroIDs(modalToLoad, $('input[type="radio"]:checked').val());

      #sourcestoCheck = $("div#" + refId + " div.idiom\\:zoteroURI span.value").text()
      #sourcetoCheckMainLayer = $("fieldset").first().children('div.dct\\:isReferencedBy').children().children('div.isReferencedBy').children().children('div.idiom\\:zoteroURI').children().children('span.value').text()

      #if(typeof refId != "undefined" && sourcestoCheck.match($('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()))
      #  alert "Source already given in this section. Please choose another source."
      #else if (typeof refId == "undefined" && sourcetoCheckMainLayer.match($('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()))
      #  alert "Duplication on main layer. Please choose another source."
      #else
      #  $this = currentField
      #  refId = currentField.parent().parent().attr('id')

      #  id = $('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()

      #  bibCit = $('div#' + modalToLoad + '-results input[value="' + id + '"]').next().text()
      #  bibCit = bibCit.substring((bibCit.search /] /) + 4, bibCit.length)

        #if id
        #  currentField.find('span.value + span').remove()
        #  currentField.find('span.value').text(id)
      #  counter = 1
      #  $("div#" + refId + " input#dct\\:bibliographicCitation").val(bibCit)
      #  nextValue = 0
      #  counter = 1
      #  $("h1#emptyListHeader").remove()
      #sourcestoCheck = ""
      #sourcestoCheckMainLayer = ""
    ) # close button#set
  ) # close done

checkMainLayer = (sourcetoCheckMainLayer) ->

  if(sourcetoCheckMainLayer.match($('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()))
    return false
  else
    return true

checkSubLayer = (sourcestoCheck) ->
  if(sourcestoCheck.match($('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()))
    return false
  else
    return true

gettySearch2 = (modalToLoad) ->
  keyword = $('input#' + modalToLoad + '-input').val()
  qstring = 'prefix wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#> prefix foaf: <http://xmlns.com/foaf/0.1/> prefix tgn: <http://vocab.getty.edu/tgn/> prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> prefix luc: <http://www.ontotext.com/owlim/lucene#> prefix gvp: <http://vocab.getty.edu/ontology#> prefix skosxl: <http://www.w3.org/2008/05/skos-xl#> prefix gvp_lang: <http://vocab.getty.edu/language/> prefix skos: <http://www.w3.org/2004/02/skos/core#> prefix dct: <http://purl.org/dc/terms/> select * { ?Subject luc:term "' + keyword + '"; a ?typ. ?typ rdfs:subClassOf gvp:Subject; rdfs:label ?Type. filter (?typ != gvp:Subject) optional {?Subject gvp:placeTypePreferred [gvp:prefLabelGVP [xl:literalForm ?Type1]]} optional {?Subject gvp:agentTypePreferred [gvp:prefLabelGVP [xl:literalForm ?Type2]]} optional {?Subject gvp:prefLabelGVP [xl:literalForm ?Term]} optional {?Subject gvp:parentStringAbbrev ?Parents} optional {?Subject foaf:focus/gvp:biographyPreferred/schema:description ?Descr} optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}}'

  $.ajax(
    type: 'GET'
    url: 'https://classicmayan.org/getty/sparql.json',
    data: 'query=' + encodeURIComponent(qstring),
    headers: {
        Accept : 'application/sparql-results+json',
    },
    cache: false
  ).done((data) ->

    results = data['results']['bindings']

    for result in results
      id = result['Subject']['value']
      term = result['Term']['value']
      parents = result['Parents']['value']
      type = result['Type']['value']
      if(typeof result['Type1'] == "undefined")
        type1 = " ? "
      else
        type1 = result['Type1']['value']

      if(typeof result['Type2'] == "undefined")
        type2 = " ? "
      else
        type2 = result['Type2']['value']

      if(typeof result['Descr'] == "undefined")
        desc = " ? "
      else
        desc = result['Descr']['value']



      $('div#' + modalToLoad + '-results').append('<div class="radio">' +
          '<label>
            <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
                <strong>' + term + ':</strong>' + " Parents: " + parents + ", Type: " + type + ", Type1: " + type1 + ", Type2: " + type2 + ", Desc: " + desc + '
           </label>
        </div>'
      )

    $("button#" + modalToLoad + "Search").prop('disabled', false)
    $("button#" + modalToLoad + "Search").text('Search')
    $('button#set-' + modalToLoad).click((e) ->
            e.preventDefault()
            id = $('div#' + modalToLoad + '-results input[name="' + modalToLoad + '-radios"]:checked').val()
            if id
                currentField.find('span.value + span').remove()
                currentField.find('span.value').text(id)
    )

  )




keywordReplacing = (keyword) ->

  keyword = keyword.replace /a/g, "[áāăąa]"
  keyword = keyword.replace /A/g, "[ĀĂĄA]"
  keyword = keyword.replace /c/g, "[ćĉċčc]"
  keyword = keyword.replace /C/g, "[ĆĈĊČC]"
  keyword = keyword.replace /d/g, "[ďđd]"
  keyword = keyword.replace /D/g, "[ĎĐD]"
  keyword = keyword.replace /e/g, "[éēĕėęěe]"
  keyword = keyword.replace /E/g, "[ĒĔĖĘĚE]"
  keyword = keyword.replace /g/g, "[ĝğġģg]"
  keyword = keyword.replace /G/g, "[ĜĞĠĢG]"
  keyword = keyword.replace /h/g, "[ĥħh]"
  keyword = keyword.replace /H/g, "[ĤĦH]"
  keyword = keyword.replace /i/g, "[íĩīĭįıi]"
  keyword = keyword.replace /I/g, "[ĨĪĬĮİI]"
  keyword = keyword.replace /j/g, "[ĳĵj]"
  keyword = keyword.replace /J/g, "[ĲĴJ]"
  keyword = keyword.replace /k/g, "[ķĸk]"
  keyword = keyword.replace /K/g, "[ĶK]"
  keyword = keyword.replace /l/g, "[łĺļľŀl]"
  keyword = keyword.replace /L/g, "[ĹĻĽĿŁL]"
  keyword = keyword.replace /n/g, "[ñńņňŉŋn]"
  keyword = keyword.replace /N/g, "[ŃŅŇŊN]"
  keyword = keyword.replace /o/g, "[óōŏőœo]"
  keyword = keyword.replace /O/g, "[ŌŎŐŒO]"
  keyword = keyword.replace /r/g, "[ŕŗřr]"
  keyword = keyword.replace /R/g, "[ŔŖŘR]"
  keyword = keyword.replace /s/g, "[śŝşšs]"
  keyword = keyword.replace /S/g, "[ŚŜŞŠS]"
  keyword = keyword.replace /t/g, "[ţťŧt]"
  keyword = keyword.replace /T/g, "[TŤŦŢ]"
  keyword = keyword.replace /u/g, "[úũūŭůűųu]"
  keyword = keyword.replace /U/g, "[ŨŪŬŮŰŲU]"
  keyword = keyword.replace /w/g, "[ŵw]"
  keyword = keyword.replace /W/g, "[ŴW]"
  keyword = keyword.replace /y/g, "[ŷy]"
  keyword = keyword.replace /Y/g, "[ŶŸY]"
  keyword = keyword.replace /z/g, "[źżžz]"
  keyword = keyword.replace /Z/g, "[ŹŹŻŽZ]"
  keyword = keyword.replace /'/g, "['‘’]"
  keyword = keyword.replace /"/g, "[\"”«»]"


lc2CJDN = ->

  $("input.idiom\\:longcount").each ->
    refId = $(this).parent().parent().parent().parent().attr('id')
    longcount = $(this).val()
    longcountValues = longcount.split "."
    calcFurther = true

    if(longcountValues.length == 1 || longcountValues.length > 5)
      calcFurther = false
    if(longcountValues.length < 5 && longcountValues.length > 1)
      alert "Value Missing. Check Longcount. Long Counts should have at least 5 values."
    else
      longcountValues = longcount.split "."
      baktun = longcountValues[0]
      katun = longcountValues[1]
      tun = longcountValues[2]
      winal = longcountValues[3]
      kin = longcountValues[4]
      longCountisValid = validateLongCount(longcount)

    baktunDays = 144000
    katunDays = 7200
    tunDays = 360
    winalDays = 20
    kinDays = 1

    baktunValue = baktun * baktunDays
    katunValue = katun * katunDays
    tunValue = tun * tunDays
    winalValue = winal * winalDays
    kinValue = kin * kinDays

    cjdn = baktunValue + katunValue + tunValue + winalValue + kinValue

    if(calcFurther == true && longCountisValid == true)
      cjdn2gd(cjdn, refId)
      cjdn2CR(cjdn, refId, baktun, katun, tun, winal, kin)

cjdn2gd = (cjdn, refId) ->
  cjdn2 = cjdn + 584283
  correlationConstAtronomic = 584283
  bce = false
  k3 = 4 * (cjdn + correlationConstAtronomic - 1721120) + 3
  x3 = parseInt(k3 / 146097, 10)

  if(x3 < 0)
    x3 = x3 - 1

  if(k3 < 0)
    k2 = 100 * parseInt(((((k3 % 146097) + 146097)) %146097 / 4),10)
  else
    k2 = 100 * parseInt(((k3 % 146097) / 4),10) + 99

  x2 = parseInt(k2 / 36525, 10)

  if(k2 < 0)
    k1 = 5 * parseInt(((((k2 % 36525) + 36525)) % 36525  / 100),10) + 2
  else
    k1 = 5 * parseInt(((k2 % 36525) / 100),10) + 2

  x1 = parseInt(k1 / 153, 10)
  c0 = parseInt((x1 + 2)/ 12, 10)

  year = 100 * x3 + x2 + c0
  month = x1 - 12 * c0 + 3

  if(k1 < 0)
    day = parseInt(((((k1 % 153) + 153)) % 153 / 5), 10) + 1
  else
    day = parseInt(((k1 % 153)) / 5, 10) + 1

  if(year < 0)
    year = --year * -1
    bce = true
  if(bce)
    ++day
  if(month < 10)
    month = "0" + month
  if(day < 10)
    day = "0" + day
  if(year < 1000 && year > 100)
    year = "0" + year

  gregdate = year + "-" + month + "-" + day

  $("div#" + refId + " input#idiom\\:gregorianDate").val(gregdate)
  if(bce)
    $("div#" + refId + " button#idiom\\:calendarEra  span[class=\"value\"]").text("BCE")
  else
    $("div#" + refId + " button#idiom\\:calendarEra  span[class=\"value\"]").text("CE")


######## Tzolkin ###########

cjdn2CR = (cjdn, refId, baktun, katun, tun, winal, kin) ->
  tzolkin = ["Ajaw", "Imix", "Ik", "Ak'bal",
               "K'an", "Chikchan", "Kimí",
               "Manik'", "Lamat", "Muluk",
               "Ok", "Chuwen", "Eb", "Ben",
               "Ix", "Men", "Kib", "Kaban",
               "Etz'nab", "Kawak"]

  if((4 + cjdn) < 0)
    newCJDN = (4 + cjdn)
    tzolkinDay = (((newCJDN) % 13 ) + 13) % 13
  else
    tzolkinDay = (4 + cjdn) % 13

    if(tzolkinDay == 0)
      tzolkinDay = tzolkinDay + 13

    if(cjdn < 0)
      tzolkinIndex = ((cjdn % 20 ) + 20) % 20
      tzolkinMonth = tzolkin[tzolkinIndex]
    else
      tzolkinMonth = tzolkin[cjdn % 20]


############ HAAB ##############

  haab = ["Pop", "Wo", "Sip", "Sotz'",
            "Tzek", "Xul", "Yaxk'in", "Mol",
            "Ch'en", "Yax", "Sak", "Keh",
            "Mac", "K'ank'in", "Muwan", "Pax",
            "K'ayab", "Kumk'u", "Wayeb"]

  kinMultiplier = 1
  winalMultiplier = 20
  tunMultiplier = -5
  katunMultiplier = -100
  baktunMultiplier = -175

  haabPreValue = (-17 + ((kin * kinMultiplier) + (winal * winalMultiplier) + (tun * tunMultiplier) + (katun * katunMultiplier) + (baktun * baktunMultiplier)))

  if (haabPreValue < 0)
    haabValue = ((haabPreValue % 365) + 365 ) % 365
  else
    haabValue = haabPreValue % 365

  haabMonth = haab[parseInt(haabValue / 20, 10)]
  haabDay = haabValue % 20

  $("div#" + refId + " input#idiom\\:tzolkinInput").val(tzolkinDay)
  $("div#" + refId + " input#idiom\\:haabInput").val(haabDay)
  $("div#" + refId + " button#idiom\\:tzolkinUnit span[class=\"value\"]").text(tzolkinMonth)
  $("div#" + refId + " button#idiom\\:haabUnit span[class=\"value\"]").text(haabMonth)


#################
## Save Object ##
#################

save = ->
  content = tgf.getInput(tgURI, originFormularType, "div#content")
  data = [content]
  saveOK = validationCheck()
  if(changedType == "Artefact")
    idiomNumber = checkIdiomNumber()
    if(idiomNumber == false)
      checkIdiomNumber()

  if(saveOK == true)
    for meta in $('[class^="additionalIdentifier"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:additionalIdentifier", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P102_has_title"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P102_has_title", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P41i_was_classified_by"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P41i_was_classified_by", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P43_has_dimension"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P43_has_dimension", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P44_has_condition"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P44_has_condition", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="isReferencedBy"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "dct:isReferencedBy", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P128_carries"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P128_carries", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="date"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "dc:date", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="prefActorAppellation"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:prefActorAppellation", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="altActorAppellation"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:altActorAppellation", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P98i_was_born"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P98i_was_born", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P100i_died_in"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P100i_died_in", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="P87_is_identified_by"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "crm:P87_is_identified_by", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasArchaeologicalCoordinates"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:hasArchaeologicalCoordinates", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasGeoreferencePoint"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:hasGeoreferencePoint", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="performedBy"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:performedBy", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="childOf"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "rel:childOf", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="grandchildOf"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "rel:grandchildOf", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="siblingOf"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "rel:siblingOf", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="spouseOf"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "rel:spouseOf", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="descendantOf"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "rel:descendantOf", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="relation"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "dbo:relation", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="divineTutelary"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:divineTutelary", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceChild"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceChild", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceGrandchild"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceGrandchild", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceSibling"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceSibling", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceSpouse"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceSibling", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceDescendant"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceDescendant", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceRelation"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceRelation", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceDivineTutelary"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceDivineTutelary", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasDiacriticFunction"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasDiacriticFunction", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasLogographicMeaning"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasLogographicMeaning", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasLogographicReading"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasLogographicReading", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasNumericFunction"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasNumericFunction", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCLLR"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCLLR", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCLLM"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCLLM", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCLSR"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCLSR", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCLDF"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCLDF", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCLNF"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCLNF", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasSyllabicReading"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasSyllabicReading", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasCatalogueEntry"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasCatalogueEntry", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasNickname"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiomcat:hasNickname", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceMaterial"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceMaterial", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="sourceTechnique"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:sourceTechnique", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasShapeAssignment"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:hasShapeAssignment", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasMaterialAssignment"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:hasMaterialAssignment", "div#"+meta.id)
      data.push(metadata)
    for meta in $('[class^="hasTechniqueAssignment"]')
      metadata = tgf.getInput(meta.id.replace('_', ':'), "idiom:hasTechniqueAssignment", "div#"+meta.id)
      data.push(metadata)



    data = JSON.stringify(data)
    $.ajax(
      type: "POST",
      url: "db/update/" + tgURI + "?tgSID=" + tgSID
      contentType: "application/ld+json",
      data: data,
      cache: false
    )

  else
    alert "Check your Entries"

#################
## Open Object ##
#################

open = (val) ->

  tgURI = val
  tgURI = tgURI.replace(/\.[0-9]*$/, "")
  openSavedContent = true
  $.ajax(
    type: "GET",
    url: "db/read/" + tgURI + "?tgSID=" + tgSID,
    mimeType: "text/turtle",
    cache: false
  ).done((data) ->

    addCall = ->
      $("div#content").empty()
      type = tgf.getType(tgURI)
      type = type.split ":"
      $("div#uriSelector").append ("<h1>" + tgURI + " : " + type[1]  + "</h1>" )
      if(type[1] == "VisualDocument")
        generateForm("idiom:VisualDocument")
      if(type[1] == "Artefact")
        generateForm("idiom:Artefact")
      if(type[1] == "E7_Activity")
        generateForm("crm:E7_Activity")
      if(type[1] == "EpigraphicInformationObject")
        generateForm("idiom:EpigraphicInformationObject")
      if(type[1] == "E8_Acquisition")
        generateForm("crm:E8_Acquisition")
      if(type[1] == "Custody")
        generateForm("idiom:Custody")
      if(type[1] == "Discovery" || type[1] == "Finding")
        generateForm("idiom:Discovery")
      if(type[1] == "E12_Production")
        generateForm("crm:E12_Production")
      if(type[1] == "E80_Part_Removal")
        generateForm("crm:E80_Part_Removal")
      if(type[1] == "NonEpigraphicGroup")
        generateForm("idiom:NonEpigraphicGroup")
      if(type[1] == "EpigraphicGroup")
        generateForm("idiom:EpigraphicGroup")
      if(type[1] == "NonEpigraphicPerson")
        generateForm("idiom:NonEpigraphicPerson")
      if(type[1] == "EpigraphicActor")
        generateForm("idiom:EpigraphicActor")
      if(type[1] == "E53_Place")
        generateForm("crm:E53_Place")
      if(type[1] == "Graph")
        generateForm("idiomcat:Graph")
      if(type[1] == "Sign")
        generateForm("idiomcat:Sign")
      if(type[1] == "Dedication")
        generateForm("idiom:Dedication")

      fillForm = ->

        tgf.fillForm(tgURI, "div#content")

        for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "crm:P102_has_title", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P102_has_title"
          formularToLoad = "crm:E35_Title"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:additionalIdentifier", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:additionalIdentifier"
          formularToLoad = "crm:E42_Identifier"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "crm:P41i_was_classified_by", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P41i_was_classified_by"
          formularToLoad = "crm:E17_Type_Assignment"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "dc:date", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dc:date"
              formularToLoad = "idiom:Date"
              addMdForm(partId, fieldName, formularToLoad)
              parentId = partId
              for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
                partId = tgf.abbrevURI(triple.object)
                fieldName = "dct:isReferencedBy"
                formularToLoad = "idiom:Source"
                addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "crm:P43_has_dimension", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P43_has_dimension"
          formularToLoad = "crm:E54_Dimension"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "crm:P44_has_condition", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P44_has_condition"
          formularToLoad = "crm:E3_Condition_State"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "crm:P128_carries", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P128_carries"
          formularToLoad = "idiom:EpigraphicInformationObject"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "idiom:hasVisualContent", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:hasVisualContent"
            formularToLoad = "crm:E36_Visual_Item"
            addMdForm(partId, fieldName, formularToLoad)
            for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)

        #for triple in tgf.getStore().find(tgURI, "dct:isReferencedBy", null)
        #  partId = tgf.abbrevURI(triple.object)
      #    fieldName = "dct:isReferencedBy"
    #      formularToLoad = "idiom:Source"
    #      addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:sourceTechnique", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:sourceTechnique"
          formularToLoad = "idiom:Source"
          addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:sourceMaterial", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:sourceMaterial"
          formularToLoad = "idiom:Source"
          addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:prefActorAppellation", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:prefActorAppellation"
          formularToLoad = "crm:E82_Actor_Appellation"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:altActorAppellation", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:altActorAppellation"
          formularToLoad = "crm:E82_Actor_Appellation"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "dc:date", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "dc:date"
          formularToLoad = "idiom:Date"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "idiom:performedBy", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:performedBy"
          formularToLoad = "idiom:PerformingActor"
          addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "crm:P100i_died_in", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P100i_died_in"
          formularToLoad = "crm:E69_Death"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "dc:date", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dc:date"
            formularToLoad = "idiom:Date"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "crm:P98i_was_born", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P98i_was_born"
          formularToLoad = "crm:E67_Birth"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "dc:date", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dc:date"
            formularToLoad = "idiom:Date"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "rel:descendantOf", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "rel:descendantOf"
          formularToLoad = "idiom:Ancestor"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(parentId, "idiom:sourceDescendant", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceDescendant"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "rel:spouseOf", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "rel:spouseOf"
          formularToLoad = "idiom:Spouse"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceSpouse", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceSpouse"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "rel:siblingOf", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "rel:siblingOf"
          formularToLoad = "idiom:Sibling"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceSibling", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceSibling"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "rel:grandchildOf", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "rel:grandchildOf"
          formularToLoad = "idiom:GrandParent"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceGrandchild", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceGrandchild"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "rel:childOf", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "rel:childOf"
          formularToLoad = "idiom:Parent"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceChild", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceChild"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:divineTutelary", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:divineTutelary"
          formularToLoad = "idiom:DivineTutelary"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceDivineTutelary", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceDivineTutelary"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "dbo:relation", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "dbo:relation"
          formularToLoad = "idiom:Relation"
          addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(partId, "idiom:sourceRelation", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiom:sourceRelation"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "crm:P87_is_identified_by", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "crm:P87_is_identified_by"
          formularToLoad = "crm:E48_Place_Name"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:hasArchaeologicalCoordinates", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:hasArchaeologicalCoordinates"
          formularToLoad = "idiom:ArchaeologicalCoordinates"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:hasGeoreferencePoint", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:hasGeoreferencePoint"
          formularToLoad = "idiom:GeoreferencePoint"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiomcat:hasSyllabicReading", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasSyllabicReading"
          formularToLoad = "idiomcat:SyllabicReading"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "idiomcat:hasCLSR", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiomcat:hasCLSR"
            formularToLoad = "idiomcat:CLSR"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiomcat:hasDiacriticFunction", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasDiacriticFunction"
          formularToLoad = "idiomcat:DiacriticFunction"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "idiomcat:hasCLDF", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiomcat:hasCLDF"
            formularToLoad = "idiomcat:CLDF"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiomcat:hasLogographicMeaning", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasLogographicMeaning"
          formularToLoad = "idiomcat:LogographicMeaning"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "idiomcat:hasCLLM", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiomcat:hasCLLM"
            formularToLoad = "idiomcat:CLLM"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiomcat:hasNumericFunction", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasNumericFunction"
          formularToLoad = "idiomcat:NumericFunction"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "idiomcat:hasCLNF", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiomcat:hasCLNF"
            formularToLoad = "idiomcat:CLNF"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "idiomcat:hasLogographicReading", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasLogographicReading"
          formularToLoad = "idiomcat:LogographicReading"
          addMdForm(partId, fieldName, formularToLoad)
          parentId = partId
          for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)
          for triple in tgf.getStore().find(parentId, "idiomcat:hasCLLR", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "idiomcat:hasCLLR"
            formularToLoad = "idiomcat:CLLR"
            addMdForm(partId, fieldName, formularToLoad)
            parentId2 = partId
            for triple in tgf.getStore().find(partId, "dct:isReferencedBy", null)
              partId = tgf.abbrevURI(triple.object)
              fieldName = "dct:isReferencedBy"
              formularToLoad = "idiom:Source"
              addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiomcat:hasCatalogueEntry", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasCatalogueEntry"
          formularToLoad = "idiomcat:CatalogueEntry"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


        for triple in tgf.getStore().find(tgURI, "idiomcat:hasNickname", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiomcat:hasNickname"
          formularToLoad = "idiomcat:Nickname"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:hasShapeAssignment", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:hasShapeAssignment"
          formularToLoad = "idiom:ShapeAssignment"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:hasTechniqueAssignment", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:hasTechniqueAssignment"
          formularToLoad = "idiom:TechniqueAssignment"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)

        for triple in tgf.getStore().find(tgURI, "idiom:hasMaterialAssignment", null)
          partId = tgf.abbrevURI(triple.object)
          fieldName = "idiom:hasMaterialAssignment"
          formularToLoad = "idiom:MaterialAssignment"
          addMdForm(partId, fieldName, formularToLoad)
          parentId2 = partId
          for triple in tgf.getStore().find(parentId2, "dct:isReferencedBy", null)
            partId = tgf.abbrevURI(triple.object)
            fieldName = "dct:isReferencedBy"
            formularToLoad = "idiom:Source"
            addMdForm(partId, fieldName, formularToLoad)


      setTimeout(fillForm, 1000)

    tgf.addTurtle(data, addCall)

  )

setSid = (val) ->
  console.log("SID: " + val)
  tgSID = val

$("li.deleteURI").click((e) ->
  e.preventDefault()
  uri=tgURI
  deleteURI(uri)
)

deleteURI = (uri) ->
  $("div.deleteRDFObjectModal").modal "show"
  $("button.deleteRDFObjectYes").click((e) ->
    e.preventDefault()
    $.ajax(
      type: "DELETE",
      url: "db/delete/" + tgURI + "?tgSID=" + tgSID,
      mimeType: "text/turtle",
      cache: false
    ).done((data) ->
      alert uri + "is deleted"
      window.close()
    )
  )


addMdForm = (fileId, fieldName, formularToLoad) ->
  divId = fileId.replace(':', '_')
  className = fileId.split ":"
  if (tgf.getStore().find(fileId, fieldName, null).length > 0)
    title = tgf.getStore().find(fileId, fieldName, null)[0].object
  else
    title = "unnamed"

  tmpl2Data = {
    fileId: fileId,
    divId: divId,
    title: title,
    metaformClass: 'dariah-aggregate'
  }

  fdiv = $('<div id="'+divId+'" class="' + className[1] + ' subformular collapse in "testitest">content</div>')
  $("span:contains(" + fileId + ")").parent().append(fdiv)
  tgf.buildForm(formularToLoad, "div#"+divId)
  tgf.fillForm(fileId, "div#"+divId)


validateGregDate = () ->
  gregDateIsValid = true

  if(typeof $("input#idiom\\:gregorianDate").val() != "undefined")

    gregDate = $("input#idiom\\:gregorianDate").val()
    if gregDate.length == 4
      if(isNaN(gregDate))
        gregDateIsValid = false
      else
        gregDateIsValid = true
    else
      gregDateParts = gregDate.split "-"
      year = gregDateParts[0]
      month = gregDateParts[1]
      day = gregDateParts[2]

      if((isNaN(year) || isNaN(month) || isNaN(day)))
        gregDateIsValid = false
      else
        gregDateIsValid = true
  else
    gregDateIsValid = true

validateLongCount = (longcount) ->

  longcountIsValid
  longcountValues = longcount.split "."

  baktun = longcountValues[0]
  katun = longcountValues[1]
  tun = longcountValues[2]
  winal = longcountValues[3]
  kin = longcountValues[4]

  if(baktun < 0 || baktun > 13 || katun < 0 || katun > 19 || tun < 0 || tun > 19 || winal < 0 || winal > 17 || kin < 0 || kin > 19)
    longcountIsValid = false
    alert "Invalid Longcount"
  else
    longcountIsValid = true

validateGeoCoordinates = () ->
  coordinatesAreValid = true
  if($("button#hasGeoreferencePoint").prev().text().length > 0)
    if(typeof $("input#wgs84pos\\:lat").val() != "undefined")
      latitude = $("input#wgs84pos\\:lat").val()

    if(typeof $("input#wgs84pos\\:long").val() != "undefined")
      longitude = $("input#wgs84pos\\:long").val()

    if(typeof $("input#wgs84pos\\:alt").val() != "undefined")
      altitude = $("input#wgs84pos\\:alt").val()


    if(isNaN(latitude) || isNaN(longitude) || isNaN(altitude))
      coordinatesAreValid = false
      alert "Only numbers are allowed. Floating values separated with \".\" (e.g. 11,87 should be 11.87)"
    else
      coordinatesAreValid = true
  else
    coordinatesAreValid = true

validateDimensions = () ->
  dimensionsAreValid
  if($("button#hasDimension").prev().text().length > 0)
    if(typeof $("input#idiom\\:height").val() != "undefined")
      ht = $("input#idiom\\:height").val()

    if(typeof $("input#idiom\\:heightLowCarving").val() != "undefined")
      hlc = $("input#idiom\\:heightLowCarving").val()

    if(typeof $("input#idiom\\:heightSculptuedArea").val() != "undefined")
      hsc = $("input#idiom\\:heightSculptuedArea").val()

    if(typeof $("input#idiom\\:heightButt").val() != "undefined")
      pb =  $("input#idiom\\:heightButt").val()

    if(typeof $("input#idiom\\:exposureAboveFloor").val() != "undefined")
      epb = $("input#idiom\\:exposureAboveFloor").val()

    if(typeof $("input#idiom\\:width").val() != "undefined")
      mw =  $("input#idiom\\:width").val()

    if(typeof $("input#idiom\\:widthBaseCarving").val() != "undefined")
      wbc = $("input#idiom\\:widthBaseCarving").val()

    if(typeof $("input#idiom\\:widthSculpturedArea").val() != "undefined")
      wsc = $("input#idiom\\:widthSculpturedArea").val()

    if(typeof $("input#idiom\\:thickness").val() != "undefined")
      mth = $("input#idiom\\:thickness").val()

    if(typeof $("input#idiom\\:depthRelief").val() != "undefined")
      rel = $("input#idiom\\:depthRelief").val()

    if(typeof $("input#idiom\\:diameter").val() != "undefined")
      dia = $("input#idiom\\:diameter").val()

    if(typeof $("input#idiom\\:perimeter").val() != "undefined")
      peri = $("input#idiom\\:perimeter").val()

    if(typeof $("input#idiom\\:weight").val() != "undefined")
      weight = $("input#idiom\\:weight").val()


    if(isNaN(ht) || isNaN(hlc) || isNaN(hsc) || isNaN(pb) || isNaN(epb) || isNaN(mw) || isNaN(wbc) || isNaN(wsc) || isNaN(mth) || isNaN(rel) || isNaN(dia) || isNaN(peri) || isNaN(weight))
      alert "Only numbers are allowed. Floating values seperated with \".\" (e.g. 11,87 should be 11.87)"
      dimensionsAreValid = false
    else
      if((ht.length > 0 || hlc.length > 0 || pb.length > 0 || epb.length > 0 || mw.length > 0 || wbc.length > 0 || wsc.length > 0 || mth.length > 0 || rel.length > 0 || dia.length > 0 || peri.length > 0) && $('button#idiom\\:hasUnit').children().text().length == 0)
        alert "Please give a unit"
        $('button#idiom\\:hasUnit').attr("style", "border:1px solid #ff0000")
        dimensionsAreValid = false
      else
        $('button#idiom\\:hasUnit').removeAttr("style")
        dimensionsAreValid = true
  else
    dimensionsAreValid = true



validationCheck = () ->
  validate1 = validateGregDate()
  validate3 = validateGeoCoordinates()
  validate4 = validateDimensions()
  validate5 = checkMandatory()
  validate6 = checkMandatoryButton()
  validate7 = checkMandatoryDropdown()

  vaildated
  if(validate1 == true && validate3 == true && validate4 == true && validate5 == true && validate6 == true && validate7 == true)
    vaildated = true
  else
    validated = false

checkIdiomNumber = () ->
  idiomNumberFilled = ""
  if($("input#idiom\\:identifier").val().length == 0)
    $.ajax(
      type: 'GET',
      url: "https://classicmayan.org/nd/noidu_idiom?mint+1",
      dataType: 'text',
      cache: 'false',
      crossDomain: true,
      beforeSend: (xhr) ->
        xhr.setRequestHeader 'Authorization', 'Basic ' + btoa('idiomnumber:!D!OM3')
      ).done((xhr) ->
        id = xhr.replace /id: /, ""
        $(".idiom\\:identifier input").val(id)
      )
    idiomNumberFilled = false
  else
    idiomNumberFilled = true



queryPrefixes = "prefix dct: <http://purl.org/dc/terms#>
              prefix dc: <http://purl.org/dc/elements/1.1/>
              prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
              prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
              prefix owl: <http://www.w3.org/2002/07/owl#>
              prefix xsd: <http://www.w3.org/2001/XMLSchema#>
              prefix idi: <http://idiom.uni-bonn.de/terms#>
              prefix idiom: <http://idiom-projekt.de/schema/>
              prefix ecrm: <http://erlangen-crm.org/current#>
              prefix edm: <http://www.europeana.eu/schemas/edm#>
              prefix form: <http://rdd.sub.uni-goettingen.de/rdfform#>
              prefix crm: <http://erlangen-crm.org/current/>
              prefix tgforms: <http://www.tgforms.de/terms#>
              prefix textgrid: <http://textgridrep.de/>
              prefix schema: <http://schema.org/>
              prefix skos: <http://www.w3.org/2004/02/skos/core#>
              prefix wgs84pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>
              prefix rel: <http://purl.org/vocab/relationship/>
              prefix edm: <http://www.europeana.eu/schemas/edm/>
prefix idiomcat: <http://idiom-projekt.de/catalogue/>"


$(document).on("ready click", "[class^='hasCLDF'] input", (e) ->

  divid =  $(this).parent().parent().parent().parent().attr('id')

  f = $("div#" + divid + " div.idiomcat\\:neverFreeStanding input").is(':checked')
  c = $("div#" + divid + " div.idiomcat\\:complementationInterference input").is(':checked')
  p = $("div#" + divid + " div.idiomcat\\:phoneticSubstitution input").is(':checked')

  level = 0

  if f == true
    level = "1"
  else
    if (c == true || p == true)
      level = "2"
    else
      level = "The given values aren't sufficient to calculate a confidence level"

  $('div#' + divid + ' input#idiomcat\\:resultsInLevel').val(level)
)

$(document).on("ready click", "[class^='hasCLLR'] input", (e) ->
  divid =  $(this).parent().parent().parent().parent().attr('id')
  level = 0
  d = $("div#" + divid + " div.idiomcat\\:colonialSources input").is(':checked')
  k = $("div#" + divid + " div.idiomcat\\:completePhoneticSubstitution input").is(':checked')
  t = $("div#" + divid + " div.idiomcat\\:partialPhoneticSubstitution input").is(':checked')
  h = $("div#" + divid + " div.idiomcat\\:homophonicSubstitution input").is(':checked')
  a = $("div#" + divid + " div.idiomcat\\:analogousSubstitution input").is(':checked')
  o = $("div#" + divid + " div.idiomcat\\:preposedComplementation input").is(':checked')
  c = $("div#" + divid + " div.idiomcat\\:postponedComplementation input").is(':checked')
  g = $("div#" + divid + " div.idiomcat\\:problematicPostponedSuffixation input").is(':checked')
  u = $("div#" + divid + " div.idiomcat\\:preconsonantalErgativePronoun input").is(':checked')
  y = $("div#" + divid + " div.idiomcat\\:prevocalicErgativePronoun input").is(':checked')
  p = $("div#" + divid + " div.idiomcat\\:expectedPartOfSpeech input").is(':checked')
  l = $("div#" + divid + " div.idiomcat\\:glLanguages input").is(':checked')
  m = $("div#" + divid + " div.idiomcat\\:otherMayanLanguages input").is(':checked')
  n = $("div#" + divid + " div.idiomcat\\:nonMayanLanguages input").is(':checked')
  i = $("div#" + divid + " div.idiomcat\\:semanticCorrespondenceWithGraphIcon input").is(':checked')
  s = $("div#" + divid + " div.idiomcat\\:semanticCorrespondenceWithContext input").is(':checked')

  level = 0
  if d == true || h == true || (k == true && p == true)
    level = "1"
  else
    level1 = false
    if ((o == true && c ==   true) || (y == true && c == true)) || t == true
      level = "2"
    else
      level2 = false
      if u == true && c == true && (( l == true || m == true || n == true )) && s == true && i == true
        level = "3"
      else
        level3 = false
        if ((a == true || o == true || c == true )) && (l == true || m == true || n == true) && s == true && p == true
          level = "4"
        else
          level4 = false
          if g == true && p == true && s == true && (l == true || m == true || n == true)
            level = "5"
          else
            level = 0
            if g == true && s == true && p == true
              level = "6"
            else
              level = 0
              if (y == true || u == true) && i == true && s == true
                level = "7"
              else
                level = 0
                if i == true && s == true
                  level = "8"
                else
                  level = "0"

  divid =  $(this).parent().parent().parent().parent().attr('id')
  $('div#' + divid + ' input#idiomcat\\:resultsInLevel').val(level)
)


$(document).on("ready click", "[class^='hasCLLM'] input", (e) ->

  divid =  $(this).parent().parent().parent().parent().attr('id')

  e = $("div#" + divid + " div.idiomcat\\:nonTextualContextOrRelation input").is(':checked')
  c = $("div#" + divid + " div.idiomcat\\:sourceComparativeContext input").is(':checked')
  a = $("div#" + divid + " div.idiomcat\\:analogousStatement input").is(':checked')
  f = $("div#" + divid + " div.idiomcat\\:consecutiveStatement input").is(':checked')
  r = $("div#" + divid + " div.idiomcat\\:causalStatement input").is(':checked')
  i = $("div#" + divid + " div.idiomcat\\:semanticCorrespondenceWithGraphIcon input").is(':checked')
  s = $("div#" + divid + " div.idiomcat\\:semanticCorrespondenceWithNonTextualInformation input").is(':checked')
  p = $("div#" + divid + " div.idiomcat\\:expectedPartOfSpeech input").is(':checked')
  b = $("div#" + divid + " div.idiomcat\\:broadStructuralSemantics input").is(':checked')
  n = $("div#" + divid + " div.idiomcat\\:narrowStructuralSemantics input").is(':checked')


  level = 0
  if e == true || c == true || a == true || f == true || r == true || i == true || s == true || p == true || b == true || n == true
    level = "1"
  else
    level = "0"

  divid =  $(this).parent().parent().parent().parent().attr('id')
  $('div#' + divid + ' input#idiomcat\\:resultsInLevel').val(level)
)





$(document).on("ready click", "[class^='hasCLSR'] input", (e) ->
  divid =  $(this).parent().parent().parent().parent().attr('id')
  #e.preventDefault()
  l = $("div#" + divid + " div.idiomcat\\:consonantInLandasAlphabet input").is(':checked')
  d = $("div#" + divid + " div.idiomcat\\:landasManuscript input").is(':checked')
  k = $("div#" + divid + " div.idiomcat\\:completeLogographicSubstitution input").is(':checked')
  t = $("div#" + divid + " div.idiomcat\\:partialLogographicSubstitution input").is(':checked')
  a = $("div#" + divid + " div.idiomcat\\:allographicSubstitution input").is(':checked')
  o = $("div#" + divid + " div.idiomcat\\:preposedToDecipheredLogogram input").is(':checked')
  c = $("div#" + divid + " div.idiomcat\\:postposedToDecipheredLogogram input").is(':checked')
  f = $("div#" + divid + " div.idiomcat\\:stemInitial input").is(':checked')
  m = $("div#" + divid + " div.idiomcat\\:stemMedial input").is(':checked')
  g = $("div#" + divid + " div.idiomcat\\:wordMedial input").is(':checked')
  r = $("div#" + divid + " div.idiomcat\\:wordFinal input").is(':checked')
  y = $("div#" + divid + " div.idiomcat\\:ergativeSpellings input").is(':checked')
  s = $("div#" + divid + " div.idiomcat\\:correspondenceWithImage input").is(':checked')
  q = $("div#" + divid + " div.idiomcat\\:correspondenceWithObject input").is(':checked')
  b = $("div#" + divid + " div.idiomcat\\:doublingIsPossible input").is(':checked')
  v = $("div#" + divid + " div.idiomcat\\:vowelHarmony input").is(':checked')
  i = $("div#" + divid + " div.idiomcat\\:lexicalCorrespondenceWithGraphIcon input").is(':checked')
  u = $("div#" + divid + " div.idiomcat\\:vowelInLandasAlphabet input").is(':checked')
  x = $("div#" + divid + " div.idiomcat\\:createsProducitveReading input").is(':checked')

  level = 0
  if d==true || u==true || (k == true && (f==true || g==true)) || (a==true && (f==true || m==true || y==true)) || ((o == true || m==true || y==true) && (g==true || r==true || c==true) && (s==true || q==true || x==true))
    level = "1"
  else
    level1 = false
    if ((t == true || o == true || c == true) && v == true && (s == true || q == true)) || ((o == true || f == true || m == true) && (g == true || r == true || c == true)) || ((g == true || r == true || c == true) && (s == true || q == true)) || ((f == true || m == true ) && (s == true || q == true))
      level = "2"
    else
      level2 = false
      if (t == true || c  == true || r  == true || l == true ) && v == false
        level = "3"
      else
        level3 = false
        if (g == true && m == true) || (m == true  && v == true) || ( r == true && v = true)
          level = "4"
        else
          level = "0"

  $('div#' + divid + ' input#idiomcat\\:resultsInLevel').val(level)

)



$(document).on("ready click", "[class^='hasCLNF'] input", (e) ->
  divid =  $(this).parent().parent().parent().parent().attr('id')
  b = $("div#" + divid + " div.idiomcat\\:barAndDotNotation input").is(':checked')
  c = $("div#" + divid + " div.idiomcat\\:calendricalContext input").is(':checked')
  a = $("div#" + divid + " div.idiomcat\\:headVariantNotation input").is(':checked')
  o = $("div#" + divid + " div.idiomcat\\:otherContext input").is(':checked')
  s = $("div#" + divid + " div.idiomcat\\:specialNotation input").is(':checked')

  level = 0
  if b == true || c == true || a == true || o == true || s == true
    level = "1"

  $('div#' + divid + ' input#idiomcat\\:resultsInLevel').val(level)

)






$(document).on("ready click", "div.idiomcat\\:variationType li", (e) ->
    variationType = $("div.idiomcat\\:variationType button span.value").text()
    e.preventDefault()
    signNr = $("div.idiomcat\\:isGraphOf span.value").text()
    if signNr.match(/^http/)
       signNr = "textgrid:" + signNr.substring("http://textgridrep.de/".length, signNr.length)
    if(signNr.length > 0)
      request = {
        'query' : queryPrefixes + 'SELECT ?signNr WHERE { GRAPH <' + signNr + '> { ?sign rdf:type idiomcat:Sign. ?sign idiomcat:signNumber ?signNr. } }'
      }

      $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        data: request
      ).done((data) ->
        signNr2 = data['results']['bindings']['0']['signNr']['value']
        graphNr = signNr2 + variationType
        $("input#idiomcat\\:graphNumber").val(graphNr)
        return graphNr
      )
)

$(document).on("ready click", "div.dc\\:date span.icon-plus", (e) ->


  if $('div.dc\\:date').length > 0
    stringSearch = $('div.dc\\:date').parent().parent().attr('class')
    if stringSearch.match("P98i_was_born")
      refId = $(this).parent().parent().parent().attr('id')
      if $('div#' + refId).children().children('div.dc\\:date').length > 2
        alert "More then two dates aren't possible"
        $('div#' + refId).children().children('div.dc\\:date').last().remove()

  if $('div.dc\\:date').length > 2
    alert "More then two date aren't possible"
    $('div.dc\\:date').last().remove()
    return false
  else
    return true
)




handleVocabulary = (modalToLoad) ->

  query = getVocabulary(modalToLoad)
  url= 'https://www.classicmayan.org/trip/vocabularies/query'

  request = {
      'query' : queryPrefixes + query
  }

  $.ajax(
      type: 'GET',
      url: url,
      cache: false,
      dataType: "json",
      data: request
  ).done((data) ->
    results = data['results']['bindings']
    appendVoc(results, modalToLoad)
  )


$(document).on("ready click", "button#reloadCitation", (e) ->
  e.preventDefault()
  zoteroKey = $(this).parent().find("span.value").text()
  divid = $(this).parent().parent().parent().parent().attr('id')
  zoteroKey = zoteroKey.substring("https://www.zotero.org/idiom_bibliography/items/".length, zoteroKey.length)

  if zoteroKey.length != 0
    $.ajax(
      type: 'GET',
      url: 'https://api.zotero.org/users/1757564/items?itemKey=' + zoteroKey + '&format=json&key=dayzvtbgcHajqLEqkB0WAj72',
      cache: false,
      dataType:'json'
    ).done((data) ->

      if data.length > 0
        for dates in data
          authors = data['0']['data']['creators']

          title = data['0']['data']['title'] + ". "
          date = " (" + data['0']['data']['date'] + "): "
          itemType = data['0']['data']['itemType']

          authorsDisp = []
          editorsDisp = []
          authorCount = 0

          for author in authors

            if authorCount == 0
              if author.creatorType == "author" || author.creatorType == "cartographer" || author.creatorType == "contributor"
                authorsDisp.push " " + author.lastName + ", " + author.firstName
              if author.creatorType == "editor"
                if authorsDisp.length == 0
                  editorsDisp.push " " + author.lastName + ", " + author.firstName

            else
                if author.creatorType == "author"
                  authorsDisp.push " " + author.firstName + " " + author.lastName
                if author.creatorType == "editor"
                  editorsDisp.push " " + author.firstName + " " + author.lastName
            if author.creatorType == "author"
              authorCount++

          if itemType == "journalArticle"
            if data['0']['data']['volume'].length == 0
              volume = ""
            else
              volume = data['0']['data']['volume'] + ": "

            if data['0']['data']['publicationTitle'].length == 0
              publicationTitle = ""
            else
              publicationTitle = data['0']['data']['publicationTitle'] + " "

            if data['0']['data']['pages'].length == 0
              pages = ""
            else
              pages = data['0']['data']['pages'] + ". "

            citation = authorsDisp + date + title + publicationTitle + volume + pages

          if itemType == "blogPost" || itemType == "encyclopediaArticle" || itemType == "webpage"
            if data['0']['data']['url'].length == 0
              url = ""
            else
              url = data['0']['data']['url']

            if data['0']['data']['blogTitle'].length == 0
              blogTitle = ""
            else
              blogTitle = data['0']['data']['blogTitle'] + ". "

            if data['0']['data']['dateAdded'].length == 0
              accessed = ""
            else
              accessed = " [" + data['0']['data']['dateAdded'] + "]"
            citation = authorsDisp + date +  title + " Electronic Document. " + blogTitle + url + accessed

          if itemType == "book"
            if data['0']['data']['publisher'].length == 0
              publisher == ""
            else
              publisher = data['0']['data']['publisher'] + ", "

            if data['0']['data']['place'].length == 0
              publishingPlace = ""
            else
              publishingPlace = data['0']['data']['place'] + "."

            if data['0']['data']['seriesNumber'].length == 0
              seriesNumber = ""
            else
              seriesNumber = " " + data['0']['data']['seriesNumber'] + ". "
            if seriesNumber.length == 3
              seriesNumber = ""
            if data['0']['data']['series'].length == 0
              series = ""
            else
              series = data['0']['data']['series']

            if data['0']['data']['volume'].length == 0
              volume = ""
            else
              volume = "(" + data['0']['data']['volume'] + ")."

            if authorsDisp.length == 0
              authorsDisp = editorsDisp
            citation = authorsDisp + date + title + series + seriesNumber + volume + publisher + publishingPlace

          if itemType == "bookSection"
            if data['0']['data']['volume'].length == 0
              volume = ""
            else
              volume = ". Vol. "  + data['0']['data']['volume']
            if data['0']['data']['series'].length == 0
              series = ""
            else
              series = data['0']['data']['series']
            if data['0']['data']['seriesNumber'].length == 0
              seriesNumber = ""
            else
              seriesNumber = " " + data['0']['data']['seriesNumber'] + ". "
            if data['0']['data']['bookTitle'].length == 0
              bookTitle = ""
            else
              bookTitle = " In: " + data['0']['data']['bookTitle']
            if data['0']['data']['pages'].length == 0
              pages = ""
            else
              pages = ", pp. " + data['0']['data']['pages'] + ". "
            if typeof publisher == "undefined"
              publisher = ""
            else
              publisher = data['0']['data']['publisher'] + ", "
            if data['0']['data']['place'].length == 0
              publishingPlace = ""
            else
              publishingPlace = data['0']['data']['place'] + "."

            if authorsDisp.length == 0
              authorsDisp = editorsDisp
            citation = authorsDisp + date + title + bookTitle + volume + ", edited by: " + editorsDisp + pages + series + seriesNumber + publisher + publishingPlace

          if itemType == "manuscript"
            manuscriptType = data['0']['data']['manuscriptType'] + ". "
            callNumber = data['0']['data']['callNumber'] + "."
            archive = data['0']['data']['archive'] + ". "
            citation = authorsDisp + date + title + manuscriptType + archive + callNumber

          if itemType == "thesis"
            thesisType = data['0']['data']['thesisType'] + ", "
            university = data['0']['data']['place'] + ". "
            place = data['0']['data']['university'] + ", "
            url = data['0']['data']['url']
            citation = authorsDisp + date + title + thesisType + place + university + url

          if itemType == "magazineArticle"
            publicationTitle = data['0']['data']['publicationTitle'] + " "
            volume = data['0']['data']['volume'] + ": "
            pages = data['0']['data']['pages'] + "."
            citation = authorsDisp + date + title + publicationTitle + volume + pages

          if itemType == "newspaperArticle"
            publicationTitle = data['0']['data']['publicationTitle'] + " "
            section = data['0']['data']['section'] + ": "
            pages = data['0']['data']['pages'] + "."
            citation = authorsDisp + date + title + publicationTitle + section + pages

          if itemType == "report"
            if data['0']['data']['reportType'].length == 0
              reportType = ""
            else
              reportType = data['0']['data']['reportType'] + ". "
              institution = data['0']['data']['institution'] + ", "
              place = data['0']['data']['place'] + ". "
              url = data['0']['data']['url']
              if url.length > 0
                dateAdded = data['0']['data']['dateAdded']
                url = " " + url + ". [" + data['0']['data']['dateAdded'] + "]"
                citation = authorsDisp + date + title + reportType + institution + place  + url
          if itemType == "document"
            url = data['0']['data']['url']
            if url.length > 0
              dateAdded = data['0']['data']['dateAdded']
              url = " " + url + ". [" + data['0']['data']['dateAdded'] + "]"
              citation = authorsDisp + date + title + url

          if itemType == "map"
            place = data['0']['data']['place'] + ". "
            url = data['0']['data']['url']
            if url.length > 0
              dateAdded = data['0']['data']['dateAdded']
              url = " " + url + ". [" + data['0']['data']['dateAdded'] + "]"
            publisher = data['0']['data']['publisher'] + ", "
            citation = authorsDisp + ": " + title + publisher + place + url

         $('div#' + divid + ' div.dct\\:bibliographicCitation input').val(citation)

    )
  else
    alert "No Zotero Entry selected"
)





$(document).on("ready click", "li#epigraphicActorReasearch", (e) ->
    #testarr = [{i: 5}, {i:9}, {i:-4, b:-99}]
    #testarr.push {i:1000}

    e.preventDefault()
    $('div#epigraphicActorSearchQuery-results').empty()
    $('div#epigraphicActorSearchQuery').modal 'show'
    request = {
        'query' : queryPrefixes + 'SELECT ?prefActorName ?citation WHERE { GRAPH <textgrid:255jf> {?tgObject idiom:prefActorAppellation ?prefActorRef. ?prefActorRef idiom:actorName ?prefActorName. ?prefActorRef dct:isReferencedBy ?sourceRef.
    ?sourceRef dct:bibliographicCitation ?citation. }}'
    }
    prefActorName = ""
    prefActorCitations = []
    prefActorCitations2 = []
    altActorRefs = []
    alternativeCitation = []

    #GET ACTOR PREF NAME AND SOURCES

    $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
    ).done((data) ->
        results = data['results']['bindings']
        for result in results
          prefActorName = result['prefActorName']['value']
          prefActorCitations.push result['citation']['value']
          year = result['citation']['value'].substring(result['citation']['value'].indexOf("(")+1, result['citation']['value'].indexOf(")"))
          prefActorCitations2.push citation:result['citation']['value'], year:result['citation']['value'].substring(result['citation']['value'].indexOf("(")+1, result['citation']['value'].indexOf(")"))

        $('div#epigraphicActorSearchQuery-results').append('<div class="prefActorname"><strong>' + prefActorName + ' (Prefered Name)</strong></div>')
        for source in prefActorCitations
          $('ul#sourcesPrefLabel-results').append('<li>' + source + '</li>')

        #GET ALTERNATIVE ACTOR REFS

        request = {
          'query' : queryPrefixes + 'SELECT DISTINCT ?altActorRef WHERE { GRAPH <textgrid:255jf> {?tgObject idiom:altActorAppellation ?altActorRef.}}'
        }

        $.ajax(
          type: 'GET',
          url: sparqlEndpoint,
          dataType: "json",
          async: false,
          data: request
        ).done((data) ->
            results = data['results']['bindings']
            for result in results
              altActorRef = result['altActorRef']['value'].substring("http://idiom-projekt.de/schema/".length, result['altActorRef']['value'].length)
              $('div.prefActorNameAndSources').after().append('<div id="' + altActorRef + '"><strong></strong><ul id="' + altActorRef + '"></div><br/>')
              altActorRefs.push altActorRef
        )

    )
    for actorRefUri in altActorRefs

          request = {
                      'query' : queryPrefixes + 'SELECT ?citation ?altActorName WHERE { GRAPH <textgrid:255jf> {idiom:' + actorRefUri + '  dct:isReferencedBy ?sourceRef. ?sourceRef dct:bibliographicCitation ?citation. idiom:' + actorRefUri +  ' idiom:actorName ?altActorName. }}'
          }

          $.ajax(
              type: 'GET',
              url: sparqlEndpoint,
              dataType: 'json',
              async: false,
              data: request
          ).done((data) ->
              results = data['results']['bindings']
              for result in results
                if($('div#' + actorRefUri).text() < 1)
                  $("div#" + actorRefUri + " strong").append(result['altActorName']['value'])
                alternativeCitation.push result['citation']['value']
              for cit in  alternativeCitation
                $('ul#' + actorRefUri).append('<li>' + cit + '</li>')
              alternativeCitation = []
          )


)

$(document).on("ready click", "li#signRelatedGraphs", (e) ->
  $('div#signRelatedGraphsQuery').modal 'show'
  request = {
        'query' : queryPrefixes + 'SELECT
(group_concat( distinct ?signNumber;separator="; ") as ?signNumbers)
(group_concat( distinct ?signstatus;separator="; ") as ?signstatuss)
(group_concat( distinct ?graphdata;separator=" || ") as ?graphdatas)
(group_concat( distinct ?srData;separator=" || ") as ?srDatas)
(group_concat( distinct ?lrData;separator=" || ") as ?lrDatas)

  WHERE { GRAPH ?tgURI {
    ?sign ^idiomcat:isGraphOf ?graph.
    BIND(IRI(CONCAT(' + "<" + ',?sign,' + ">" + ')) AS ?signURI)
    GRAPH ?signURI {
      ?sign idiom:status ?signstatus.
      ?sign idiomcat:signNumber ?signNumber.
      OPTIONAL{
        ?sign idiomcat:hasSyllabicReading ?syllabicReadingRef.
      BIND(IRI(CONCAT(' + "<" + ',?syllabicReadingRef,' + ">" + ')) AS ?syllabicReadingURL)
        GRAPH ?syllabicReadingURL {
        	?syllabicReadingRef idiom:status ?syllabicReadingStatus.
        	?syllabicReadingRef idiomcat:transliterationValue ?transValueSR.
      		OPTIONAL {
        		?syllabicReadingRef idiomcat:hasCLSR ?clsrRef.
          		BIND(IRI(CONCAT(' + "<" + ',?clsrRef,' + ">" + ')) AS ?clsrURL)
          		GRAPH ?clsrURL {
            		?clsrRef idiom:status ?clsrStatus.
      				?clsrRef idiomcat:resultsInLevel ?clsrValue.
          		}
          	}
        BIND((CONCAT(STR(?transValueSR), " [", STR(?syllabicReadingStatus), "] | Confidence: ", STR(?clsrValue), " [", STR(?clsrStatus), "]" )) AS ?srData)
        }}
        OPTIONAL { ?sign idiomcat:hasLogographicReading ?logographicReadingRef.
        BIND(IRI(CONCAT(' + "<" + ',?logographicReadingRef,' + ">" + ')) AS ?logographicReadingURL)
        GRAPH ?logographicReadingURL {
        	?logographicReadingRef idiom:status ?logographicReadingStatus.
        	?logographicReadingRef idiomcat:transliterationValue ?transValueLR.
      		OPTIONAL {
        		?logographicReadingRef idiomcat:hasCLLR ?cllrRef.
          		BIND(IRI(CONCAT(' + "<" + ',?cllrRef,' + ">" + ')) AS ?cllrURL)
          		GRAPH ?cllrURL {
            		?cllrRef idiom:status ?cllrStatus.
      				?cllrRef idiomcat:resultsInLevel ?cllrValue.
          		}
          	}
        BIND((CONCAT(STR(?transValueLR), " [", STR(?logographicReadingStatus), "] | Confidence: ", STR(?cllrValue), " [", STR(?cllrStatus), "]" )) AS ?lrData)
      	}
      }
    }
    ?graph idiomcat:graphNumber ?graphNumber.
    ?graph idiom:status ?graphstatus.
    ?graph idiomcat:hasDigitalImage ?image.
    BIND((CONCAT(STR(?graphNumber), ", Status: ", STR(?graphstatus), ", Image: ", STR(?image))) AS ?graphdata)

    }
  } group by ?sign order by ASC(xsd:integer(?signNumbers))'
  }

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
  ).done((data) ->
        results = data['results']['bindings']
        graphs = []
        if $( "div#signRelatedGraphsQuery .modal-header .resultLength" ).length == 0
          $('div#signRelatedGraphsQuery .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

        for result in results
          signNumber = result['signNumbers']['value']
          signStatus = result['signstatuss']['value']
          graphdata = result['graphdatas']['value']

          if (typeof result['srDatas'] == "undefined")
            srData = ""
          else
            srData = result['srDatas']['value']

          if (typeof result['lrDatas'] == "undefined")
            lrData = ""
          else
            lrData = result['lrDatas']['value']

          graphs = graphdata.split "||"

          $("ul#signRelatedGraphsQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong> Sign-Nr. ' + signNumber + ' [' + signStatus + '] ' +  '</strong></li><ul id=' + '"sign' + signNumber + '"></ul></div>')
          #$("ul#signRelatedGraphsQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong>Syllabic Reading: </strong>' + srData + '</ul></div>')
          #$("ul#signRelatedGraphsQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong>Logographic Reading: </strong>' + lrData + '</ul></div>')
          $('ul#sign' + signNumber).append('<li style="padding:5px;"><strong>Logographic Reading:</strong> ' + lrData + '</li>')
          $('ul#sign' + signNumber).append('<li style="padding:5px;"><strong>Syllabic Reading:</strong> ' + srData + '</li>')

          for graph in graphs
            imageURI = graph.split "Image: "
            imageURInorm = imageURI[1].replace /^\s+|\s+$/g, ""
            digilib = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + imageURInorm + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
            $('ul#sign' + signNumber).append('<li style="pading:5px;">' + imageURI[0] + ' ' + digilib + '</li>')
  )
)

$(document).on("ready click", "li#signRelatedGraphsSyallabicReading", (e) ->
  $('div#signRelatedGraphsSyallabicReadingQuery').modal 'show'

  request = {
        'query' : queryPrefixes + 'SELECT
(group_concat( distinct ?signNumber;separator="; ") as ?signNumbers)
(group_concat( distinct ?signstatus;separator="; ") as ?signstatuss)
(group_concat( distinct ?graphdata;separator=" || ") as ?graphdatas)
(group_concat( distinct ?transValueSR;separator=" || ") as ?transValueSRs)

  WHERE { GRAPH ?tgURI {

    ?sign ^idiomcat:isGraphOf ?graph.
    BIND(IRI(CONCAT(' + "<" + ',?sign,' + ">" + ')) AS ?signURI)
    GRAPH ?signURI {
      ?sign idiom:status ?signstatus.
      ?sign idiomcat:signNumber ?signNumber.
      ?sign idiomcat:hasSyllabicReading ?syllabicReadingRef.
      BIND(IRI(CONCAT(' + "<" + ',?syllabicReadingRef,' + ">" + ')) AS ?syllabicReadingURL)
      GRAPH ?syllabicReadingURL {
        	?syllabicReadingRef idiomcat:transliterationValue ?transValueSR.
      		OPTIONAL {
        		?syllabicReadingRef idiomcat:hasCLSR ?clsrRef.
      			?clsrRef idiomcat:resultsInLevel ?clsrValue.
          	}
      	}
    }
    ?graph idiomcat:graphNumber ?graphNumber.
    ?graph idiom:status ?graphstatus.
    ?graph idiomcat:hasDigitalImage ?image.
    BIND((CONCAT(STR(?graphNumber), ", Status: ", STR(?graphstatus), ", Image: ", STR(?image))) AS ?graphdata)
    }
  } group by ?sign ORDER BY asc(UCASE(str(?transValueSRs)))'
  }

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
  ).done((data) ->
        results = data['results']['bindings']
        graphs = []
        if $( "div#signRelatedGraphsSyallabicReadingQuery .modal-header .resultLength" ).length == 0
          $('div#signRelatedGraphsSyallabicReadingQuery .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
        for result in results
          signNumber = result['signNumbers']['value']
          signStatus = result['signstatuss']['value']
          graphdata = result['graphdatas']['value']
          graphs = graphdata.split "||"
          syllabicReading = result['transValueSRs']['value']
          confidenLevelSR = $("input#confidenLevelSR").val()
          $("ul#signRelatedGraphsSyallabicReadingQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong> Sign-Nr. ' + signNumber + ' [' + signStatus + '] ' + ' SR: ' + syllabicReading + '</strong></li><ul id=' + '"sign' + signNumber + '"></ul></div>')
          for graph in graphs
            imageURI = graph.split "Image: "
            imageURInorm = imageURI[1].replace /^\s+|\s+$/g, ""
            digilib = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + imageURInorm + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
            $('ul#sign' + signNumber).append('<li style="pading:5px;">' + imageURI[0] + ' ' + digilib + '</li>')
  )
)

$(document).on("ready click", "button#filterForCLSR", (e) ->
    $('div#signRelatedGraphsSyallabicReadingQuery').modal 'show'
    levelForFilter = $("input#confidenLevelSR").val()
    request = {
          'query' : queryPrefixes + 'SELECT
  (group_concat( distinct ?signNumber;separator="; ") as ?signNumbers)
  (group_concat( distinct ?signstatus;separator="; ") as ?signstatuss)
  (group_concat( distinct ?graphdata;separator=" || ") as ?graphdatas)
  (group_concat( distinct ?transValueSR;separator=" || ") as ?transValueSRs)

    WHERE { GRAPH ?tgURI {

      ?sign ^idiomcat:isGraphOf ?graph.
      BIND(IRI(CONCAT(' + "<" + ',?sign,' + ">" + ')) AS ?signURI)
      GRAPH ?signURI {
        ?sign idiom:status ?signstatus.
        ?sign idiomcat:signNumber ?signNumber.
        ?sign idiomcat:hasSyllabicReading ?syllabicReadingRef.
        BIND(IRI(CONCAT(' + "<" + ',?syllabicReadingRef,' + ">" + ')) AS ?syllabicReadingURL)
        GRAPH ?syllabicReadingURL {
          	?syllabicReadingRef idiomcat:transliterationValue ?transValueSR.
        		OPTIONAL {
          		?syllabicReadingRef idiomcat:hasCLSR ?clsrRef.
        			?clsrRef idiomcat:resultsInLevel ?clsrValue.
            	}
        	}
      }
      ?graph idiomcat:graphNumber ?graphNumber.
      ?graph idiom:status ?graphstatus.
      ?graph idiomcat:hasDigitalImage ?image.
      BIND((CONCAT(STR(?graphNumber), ", Status: ", STR(?graphstatus), ", Image: ", STR(?image))) AS ?graphdata)
      }
    } group by ?sign HAVING (?clsrValues = "' + levelForFilter + '") ORDER BY asc(UCASE(str(?transValueSRs)))'
    }

    $.ajax(
          type: 'GET',
          url: sparqlEndpoint,
          cache: false,
          dataType: "json",
          async: false,
          data: request
    ).done((data) ->
          results = data['results']['bindings']
          graphs = []
          if $( "div#signRelatedGraphsSyallabicReadingQuery .modal-header .resultLength" ).length == 0
            $('div#signRelatedGraphsSyallabicReadingQuery .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
          for result in results
            signNumber = result['signNumbers']['value']
            signStatus = result['signstatuss']['value']
            graphdata = result['graphdatas']['value']
            graphs = graphdata.split "||"
            syllabicReading = result['transValueSRs']['value']
            confidenLevelSR = $("input#confidenLevelSR").val()
            $("ul#signRelatedGraphsSyallabicReadingQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong> Sign-Nr. ' + signNumber + ' [' + signStatus + '] ' + ' SR: ' + syllabicReading + '</strong></li><ul id=' + '"sign' + signNumber + '"></ul></div>')
            for graph in graphs
              imageURI = graph.split "Image: "
              imageURInorm = imageURI[1].replace /^\s+|\s+$/g, ""
              digilib = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + imageURInorm + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
              $('ul#sign' + signNumber).append('<li style="pading:5px;">' + imageURI[0] + ' ' + digilib + '</li>')
    )
)


$(document).on("ready click", "li#signRelatedGraphsLogographicReading", (e) ->
  $('div#signRelatedGraphsLogographicReadingQuery').modal 'show'
  request = {
        'query' : queryPrefixes + 'SELECT
(group_concat( distinct ?signNumber;separator="; ") as ?signNumbers)
(group_concat( distinct ?signstatus;separator="; ") as ?signstatuss)
(group_concat( distinct ?graphdata;separator=" || ") as ?graphdatas)
(group_concat( distinct ?transValueLR;separator=" || ") as ?transValueLRs)

  WHERE { GRAPH ?tgURI {

    ?sign ^idiomcat:isGraphOf ?graph.
    BIND(IRI(CONCAT(' + "<" + ',?sign,' + ">" + ')) AS ?signURI)
    GRAPH ?signURI {
      ?sign idiom:status ?signstatus.
      ?sign idiomcat:signNumber ?signNumber.
      ?sign idiomcat:hasLogographicReading ?logographicReadingRef.
      BIND(IRI(CONCAT(' + "<" + ',?logographicReadingRef,' + ">" + ')) AS ?logographicReadingURL)
      GRAPH ?logographicReadingURL {
        	?logographicReadingRef idiomcat:transliterationValue ?transValueLR.
      		OPTIONAL {
        		?logographicReadingRef idiomcat:hasCLLR ?cllrRef.
      			?cllrRef idiomcat:resultsInLevel ?cllrValue.
          	}
      	}
    }
    ?graph idiomcat:graphNumber ?graphNumber.
    ?graph idiom:status ?graphstatus.
    ?graph idiomcat:hasDigitalImage ?image.
    BIND((CONCAT(STR(?graphNumber), ", Status: ", STR(?graphstatus), ", Image: ", STR(?image))) AS ?graphdata)
    }
  } group by ?sign ORDER BY asc(UCASE(str(?transValueLRs)))'
  }

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
  ).done((data) ->
        results = data['results']['bindings']
        graphs = []
        if $( "div#signRelatedGraphsLogographicReadingQuery .modal-header .resultLength" ).length == 0
          $('div#signRelatedGraphsLogographicReadingQuery .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
        for result in results
          signNumber = result['signNumbers']['value']
          signStatus = result['signstatuss']['value']
          graphdata = result['graphdatas']['value']
          graphs = graphdata.split "||"
          logographicReading = result['transValueLRs']['value']

          $("ul#signRelatedGraphsLogographicReadingQuery-results").append('<div style="border-bottom:1px groove #ffffff; padding-top:10px;"><li>' + '<strong> Sign-Nr. ' + signNumber + ' [' + signStatus + '] ' + ' LR: ' + logographicReading + '</strong></li><ul id=' + '"sign' + signNumber + '"></ul></div>')
          for graph in graphs
            imageURI = graph.split "Image: "
            imageURInorm = imageURI[1].replace /^\s+|\s+$/g, ""
            digilib = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + imageURInorm + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
            $('ul#sign' + signNumber).append('<li style="pading:5px;">' + imageURI[0] + ' ' + digilib + '</li>')
  )
)



$(document).on("ready click", "li#signRelatedSigns", (e) ->
  $('div#signRelatedSignsQuery').modal 'show'
  request = {
        'query' : queryPrefixes + 'SELECT  ?signNumber  ?subjectStatus ?isSameAsSign ?isSameAsSignStatus ?isDistinctAllographOf
WHERE { GRAPH ?textgridURI {
    OPTIONAL {?tgObject ^idiomcat:isSameAs ?sharedElementGraph.
    		?sharedElementGraph idiomcat:signNumber ?signNumber.
             ?sharedElementGraph idiom:status ?subjectStatus
    	}
    BIND(IRI(CONCAT(' + "<" + ',?tgObject,' + ">" + ')) AS ?tgObjectURI)
    GRAPH ?tgObjectURI {
      ?tgObject idiom:status ?isSameAsSignStatus.
      ?tgObject idiomcat:signNumber ?isSameAsSign.
    }
    OPTIONAL {?tgObject ^idiomcat:isDistinctAllographOf ?isDistinctAllographOf.}
  }}'
  }

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
  ).done((data) ->
        results = data['results']['bindings']
        graphs = []
        for result in results
          signNumber = result['signNumber']['value']
          subjectStatus = result['subjectStatus']['value']
          isSameAsSign = result['isSameAsSign']['value']
          isSameAsSignStatus = result['isSameAsSignStatus']['value']
          if typeof result['isDistinctAllographOf'] == "undefined"
            isDistinctAllographOf = ""
          else
            isDistinctAllographOf = result['isDistinctAllographOf']['value']


          $("ul#signRelatedSignsQuery-results").append('<li>' + '<strong> Sign-Nr. ' + signNumber + ' [' + subjectStatus + '] ' +  '</strong> is same as Sign: ' + isSameAsSign + " [" + isSameAsSignStatus + "]"+ '</li>')

  )
)


$(document).on("ready click", "li#graphRelatedGraphs", (e) ->
  $('div#graphRelatedGraphsQuery').modal 'show'
  request = {
        'query' : queryPrefixes + 'SELECT
 (group_concat( distinct ?subjectGraphNumber;separator="; ") as ?subjectGraphNumbers)
(group_concat( distinct ?subjectStatus;separator="; ") as ?subjectStatuss)
(group_concat( distinct ?subjectGraphImage;separator="; ") as ?subjectGraphImages)
(group_concat( ?objectGraphData;separator=" || ") as ?objectGraphDatas)


WHERE { GRAPH ?textgridURI {?tgObject ^idiomcat:sharesDiagnosticElementsWith ?sharedElementGraph.
   BIND(IRI(CONCAT(' + "<" + ',?tgObject,' + ">" + ')) AS ?tgObjectURI)
    GRAPH ?tgObjectURI {
      ?tgObject idiom:status ?subjectStatus.
      ?tgObject idiomcat:graphNumber ?subjectGraphNumber.
      ?tgObject idiomcat:hasDigitalImage ?subjectGraphImage.
    }

    OPTIONAL {
      ?sharedElementGraph idiomcat:graphNumber ?sharesDiagnosticElementsWithGraph.
      ?sharedElementGraph idiom:status ?status.
      ?sharedElementGraph idiomcat:hasDigitalImage ?image.
      BIND((CONCAT(STR(?sharesDiagnosticElementsWithGraph), ", Status: ", STR(?status), ", Image: ", STR(?image))) AS ?objectGraphData)
    }
  }} group by ?sharesDiagnosticElementsWithGraph'
  }

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
  ).done((data) ->
        results = data['results']['bindings']
        graphs = []
        for result in results
          #signNumber = result['signNumber']['value']
          subjectGraphNumbers = result['subjectGraphNumbers']['value']
          subjectStatuss = result['subjectStatuss']['value']
          subjectGraphImages = result['subjectGraphImages']['value']
          if typeof result['objectGraphDatas'] == "undefined"
            objectGraphDatas = ""
          else
            objectGraphDatas = result['objectGraphDatas']['value']


          $("ul#graphRelatedGraphsQuery-results").append('<li>' + '<strong> Sign-Nr. ' + subjectGraphNumbers + ' [' + subjectStatuss + '] ' +  '</strong>: ' + subjectGraphImages + " [" + objectGraphDatas + "]"+ '</li>')

  )
)



$(document).on("ready click", "li#signsNotRelatedToGraphs", (e) ->
  $('div#signsNotRelatedToGraphs').modal 'show'
  signs = []
  request = { 'query' : queryPrefixes + 'SELECT ?tgURI ?sign ?signNumber WHERE {
  GRAPH ?tgURI {
     ?sign rdf:type idiomcat:Sign.
     MINUS{
  			SELECT distinct ?sign WHERE {?sign ^idiomcat:isGraphOf ?graph.}
     }
    ?sign idiomcat:signNumber ?signNumber.
  }
}  '}
  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
   ).done((data) ->
     results = data['results']['bindings']

     for result in results
       #signs.push result['tgURI']['value']
       signNumber = result['signNumber']['value']
       tgURI = result['tgURI']['value']

       $('div.signsNotRelatedToGraphs-results').append('<div style="border-bottom:1px groove #ffffff;"><form id="sign' + signNumber + '"><strong>Sign: ' + signNumber + '</strong> || URI: ' + tgURI + '</form></div>')

   )
)

$(document).on("ready click", "li#graphWithoutSigns", (e) ->
  $('div#graphWithoutSigns').modal 'show'
  signs = []

  query = " SELECT ?graphNumber ?tgURI WHERE {
  GRAPH ?tgURI {
    ?graph rdf:type idiomcat:Graph.
    FILTER NOT EXISTS{?graph idiomcat:isGraphOf ?graphOfSign}
    ?graph idiomcat:graphNumber ?graphNumber.
  }
} "

  request = { 'query' : queryPrefixes + '' + query}
  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
   ).done((data) ->
     results = data['results']['bindings']

     for result in results
       #signs.push result['tgURI']['value']
       graphNumber = result['graphNumber']['value']
       tgURI = result['tgURI']['value']

       $('div.graphWithoutSigns-results').append('<div style="border-bottom:1px groove #ffffff;"><form id="graph' + graphNumber + '"><strong>Graph: ' + graphNumber + '</strong> || URI: ' + tgURI + '</form></div>')

   )
)

$(document).on("ready click", "li#graphWithoutImage", (e) ->
  $(this).removeData();
  $('div#graphWithoutImage').modal 'show'
  signs = []
  request = { 'query' : queryPrefixes + ' SELECT ?tgURI ?graphNumber WHERE {
  GRAPH ?tgURI {
    ?graph rdf:type idiomcat:Graph.
    FILTER NOT EXISTS{?graph idiomcat:hasDigitalImage ?graphWithoutImage}
    ?graph idiomcat:graphNumber ?graphNumber.
  }
}  '}
  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
   ).done((data) ->
     results = data['results']['bindings']

     for result in results
       #signs.push result['tgURI']['value']
       graphNumber = result['graphNumber']['value']
       tgURI = result['tgURI']['value']

       $('div.graphWithoutImage-results').append('<div style="border-bottom:1px groove #ffffff;"><form id="graph' + graphNumber + '"><strong>Graph: ' + graphNumber + '</strong> || URI: ' + tgURI + '</form></div>')

   )
)


$(document).on("ready click", "li#signsWithSignfunction", (e) ->
  $('div#signsWithSignfunction').modal 'show'
  signs = []
  # get all URIs
  request = { 'query' : queryPrefixes + 'SELECT  ?textgridURI ?signNumber WHERE { GRAPH ?textgridURI {?tgObject idiomcat:signNumber ?signNumber. ?tgObject rdf:type idiomcat:Sign.}}'}

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
   ).done((data) ->
     results = data['results']['bindings']

     for result in results
       signs.push result['textgridURI']['value']
       signNumber = result['signNumber']['value']
       $('div.signsWithSignfunction-results').append('<div style="border-bottom:1px groove #ffffff;"><form id="sign' + signNumber + '" class="form-inline"><strong>' + signNumber + '</strong></form></div>')

     for sign in signs
       getSyllabicReadings(sign)
       getLogographicReadings(sign)
       getLogographicMeanings(sign)

   )


)

getSyllabicReadings = (tgURI) ->

  request = {'query' : queryPrefixes +  " SELECT ?signNumber ?syllabicReadingTransValue ?clsrLevel ?syllabicReadingCit ?syllabicReadingCitPage WHERE { GRAPH <" + tgURI + "> {
   ?tgObject idiomcat:signNumber ?signNumber.
   OPTIONAL {
      ?tgObject idiomcat:hasSyllabicReading ?syllabicReadingRef.
      OPTIONAL {?syllabicReadingRef idiomcat:hasCLLM ?clsrRef.
        ?clsrRef idiomcat:resultsInLevel ?clsrLevel.}
    ?syllabicReadingRef idiomcat:transliterationValue ?syllabicReadingTransValue.
   OPTIONAL{  ?syllabicReadingRef dct:isReferencedBy ?sourceRef.
      ?sourceRef dct:bibliographicCitation ?syllabicReadingCit.
      ?sourceRef schema:pagination ?syllabicReadingCitPage.
      }}
  }}"}

  $.ajax(
    type: 'GET',
    url: sparqlEndpoint,
    cache: false,
    dataType: "json",
    async: false,
    data: request
  ).done((data) ->
    results = data['results']['bindings']
    for result in results
            signNumber = result['signNumber']['value']
            if typeof result['syllabicReadingTransValue'] == "undefined"
              syllabicReadingTransValue = ""
            else
              syllabicReadingTransValue = result['syllabicReadingTransValue']['value']
            if typeof result['clsrLevel'] == "undefined"
              clsrLevel = ""
            else
              cllmLevel = result['clsrLevel']['value']
            if typeof result['syllabicReadingCit'] == "undefined"
              syllabicReadingCit = ""
            else
              syllabicReadingCit = result['syllabicReadingCit']['value']
            if typeof result['syllabicReadingCitPage'] == "undefined"
              syllabicReadingCitPage = ""
            else
              syllabicReadingCitPage = result['syllabicReadingCitPage']['value']

            if $('form#sign' + signNumber + ' li.firstLayer').size() == 0
              $('form#sign' + signNumber).after().append('
                  <ul>
                    <li class="firstLayer">Syllabic Reading</li>
                      <ul>
                        <li><strong>' + syllabicReadingTransValue + "</strong> " + clsrLevel + " " + syllabicReadingCit + " " + syllabicReadingCitPage + '</li>
                      </ul>
                  </ul>')
            else
              $('form#sign' + signNumber + ' li.firstLayer').next().append(
                '<li><strong>'  + syllabicReadingTransValue + "</strong> " + clsrLevel + " " + syllabicReadingCit + " " + syllabicReadingCitPage + '</li>'
              )


  )


getLogographicReadings = (tgURI) ->

  request = {'query' : queryPrefixes +  " SELECT  ?signNumber ?logoReadingTransValue ?cllrLevel ?logoReadingCit ?logoReadingCitPage WHERE { GRAPH <" + tgURI + "> {
  ?tgObject idiomcat:signNumber ?signNumber.
  OPTIONAL {
    ?tgObject idiomcat:hasLogographicReading ?logoReadingRef.
    ?logoReadingRef idiomcat:hasCLLR ?cllrRef.
    ?cllrRef idiomcat:resultsInLevel ?cllrLevel.
    ?logoReadingRef idiomcat:transliterationValue ?logoReadingTransValue.
   OPTIONAL{  ?logoReadingRef dct:isReferencedBy ?sourceRef.
      ?sourceRef dct:bibliographicCitation ?logoReadingCit.
      ?sourceRef schema:pagination ?logoReadingCitPage.
      }}}}"}
  $.ajax(
    type: 'GET',
    url: sparqlEndpoint,
    cache: false,
    dataType: "json",
    async: false,
    data: request
  ).done((data) ->

    results = data['results']['bindings']
    for result in results
            signNumber = result['signNumber']['value']
            if typeof result['logoReadingTransValue'] == "undefined"
              logoReadingTransValue = ""
            else
              logoReadingTransValue = result['logoReadingTransValue']['value']

            if typeof result['cllrLevel'] == "undefined"
              cllrLevel = ""
            else
              cllrLevel = result['cllrLevel']['value']

            if typeof result['logoReadingCit'] == "undefined"
              logoReadingCit = ""
            else
              logoReadingCit = result['logoReadingCit']['value']
            if typeof result['logoReadingCitPage'] == "undefined"
              logoReadingCitPage = ""
            else
              logoReadingCitPage = result['logoReadingCitPage']['value']

            if $('form#sign' + signNumber + ' li.firstLayerLogographicReading').size() == 0

              $('form#sign' + signNumber).after().append('
                  <ul>
                    <li class="firstLayerLogographicReading">Logographic Reading</li>
                      <ul>
                        <li><strong>' + logoReadingTransValue + "</strong> " + cllrLevel + " " + logoReadingCit + " " + logoReadingCitPage + '</li>
                      </ul>
                  </ul>')
            else
              $('form#sign' + signNumber + ' li.firstLayerLogographicReading').next().append(
                '<li><strong>'  + logoReadingTransValue + "</strong> " + cllrLevel + " " + logoReadingCit + " " + logoReadingCitPage + '</li>'
              )
  )


getLogographicMeanings = (tgURI) ->

  request = {'query' : queryPrefixes +  " SELECT  ?signNumber ?logoMeaningTransValue ?cllmLevel ?logoMeaningCit ?logoMeaningCitPage WHERE { GRAPH <" + tgURI + "> {
  ?tgObject idiomcat:signNumber ?signNumber.
  OPTIONAL {
      ?tgObject idiomcat:hasLogographicMeaning ?logoMeaningRef.
      OPTIONAL {?logoMeaningRef idiomcat:hasCLLM ?cllmRef.
        ?cllmRef idiomcat:resultsInLevel ?cllmLevel.}
    ?logoMeaningRef idiomcat:transliterationValue ?logoMeaningTransValue.
   OPTIONAL{  ?logoReadingRef dct:isReferencedBy ?sourceRef.
      ?sourceRef dct:bibliographicCitation ?logoMeaningCit.
      ?sourceRef schema:pagination ?logoMeaningCitPage.
      }}}}"}
  $.ajax(
    type: 'GET',
    url: sparqlEndpoint,
    cache: false,
    dataType: "json",
    async: false,
    data: request
  ).done((data) ->
    results = data['results']['bindings']

    for result in results
            signNumber = result['signNumber']['value']
            if typeof result['logoMeaningTransValue'] == "undefined"
              logoMeaningTransValue = ""
            else
              logoMeaningTransValue = result['logoMeaningTransValue']['value']

            if typeof result['cllmLevel'] == "undefined"
              cllmLevel = ""
            else
              cllmLevel = result['cllmLevel']['value']

            if typeof result['logoMeaningCit'] == "undefined"
              logoMeaningCit = ""
            else
              logoMeaningCit = result['logoMeaningCit']['value']
            if typeof result['logoMeaningCitPage'] == "undefined"
              logoMeaningCitPage = ""
            else
              logoMeaningCitPage = result['logoMeaningCitPage']['value']

            if $('form#sign' + signNumber + ' li.firstLayerLogographicMeaning').size() == 0

              $('form#sign' + signNumber).after().append('
                  <ul>
                    <li class="firstLayerLogographicMeaning">Logographic Reading</li>
                      <ul>
                        <li><strong>' + logoMeaningTransValue + "</strong> " + cllmLevel + " " + logoMeaningCit + " " + logoMeaningCitPage + '</li>
                      </ul>
                  </ul>')
            else
              $('form#sign' + signNumber + ' li.firstLayerLogographicMeaning').next().append(
                '<li><strong>'  + logoMeaningTransValue + "</strong> " + cllmLevel + " " + logoMeaningCit + " " + logoMeaningCitPage + '</li>'
              )
  )


$(document).on 'ready click', 'button#listAllArtefactsAlphabeticalSearch' ,(e) ->
  $("table#artefactTable").find("tr:gt(0)").remove();
  $('input#artefactPage').val("0")
  $('div#listAllArtefactsAlphabetical').modal 'show'
  keyword = $('input#listAllArtefactsAlphabetical-input').val()
  console.log "prefiex: " + queryPrefixes
  request = {'query' : queryPrefixes + getAllArtefactsAlphabetical(keyword, 0)}

  $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      cache: false,
      dataType: "json",
      async: false,
      data: request
    ).done((data) ->
      appendArtefactList(data['results']['bindings'])
    )


$(document).on 'ready click', 'li#listAllArtefactsAlphabetical', (e) ->
  $("table#artefactTable").find("tr:gt(0)").remove();
  $('input#artefactPage').val("0")
  $('div#listAllArtefactsAlphabetical').modal 'show'
  keyword = $('input#listAllArtefactsAlphabetical-input').val()
  console.log "prefiex: " + queryPrefixes
  request = {'query' : queryPrefixes + getAllArtefactsAlphabetical(keyword, 0)}

  $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      cache: false,
      dataType: "json",
      async: false,
      data: request
    ).done((data) ->
      appendArtefactList(data['results']['bindings'])
    )

$(document).on 'ready click', 'button#nextArtefacts', (e) ->
  #$('table#artefactTable').empty()
  $("table#artefactTable").find("tr:gt(0)").remove();
  keyword=$('input#listAllArtefactsAlphabetical-input').val()
  offset = $('input#artefactPage').val()
  pagenumber = parseInt(offset) + 50
  $('input#artefactPage').val(pagenumber)

  request = {'query' : queryPrefixes + getAllArtefactsAlphabetical(keyword, pagenumber)}

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
      ).done((data) ->
        appendArtefactList(data['results']['bindings'])
  )

$(document).on 'ready click', 'button#prevArtefacts', (e) ->

  $("table#artefactTable").find("tr:gt(0)").remove();
  keyword = $('input#listAllArtefactsAlphabetical-input').val()
  offset = $('input#artefactPage').val()
  pagenumber = parseInt(offset) - 50
  $('input#artefactPage').val(pagenumber)

  request = {'query' : queryPrefixes + getAllArtefactsAlphabetical(keyword, pagenumber)}

  $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
      ).done((data) ->
        appendArtefactList(data['results']['bindings'])
  )

$(document).on 'ready click', 'li#activitiesWithActor', (e) ->
  console.log "BUTTON"
  request = undefined
  signs = undefined
  $('table#actorActiv').empty()
  $('div#activitiesWithRelatedActor').modal 'show'
  $('table#actorActiv').append('<tr>
  									<th id="longcount">Longcount</th>
  									<th id="activityTitle">Activity</th>
  									<th id="actorName">Actor (Birth - Death)</th>
  									<th id="placeName">Activity Place</th>
  									<th id="relatedActivityTitle"> Related Activity</th>
  									<th id="agentName">Actor of Related Activity</th>
  									<th id="monument">Monument</th>
  									<th id="source">Sources</th>
  								</tr>')
  signs = []
  request = 'query': queryPrefixes + getActivitiesWithActor()
  console.log getActivitiesWithActor()
  $.ajax(
    type: 'GET'
    url: sparqlEndpoint
    cache: false
    dataType: 'json'
    async: false
    data: request).done (data) ->
    activityTitle = undefined
    actorName = undefined
    agentName = undefined
    longcount = undefined
    placeName = undefined

    relatedActivityTitle = undefined
    result = undefined
    results = undefined
    source = undefined
    uri = undefined
    _i = undefined
    _len = undefined
    _results = undefined
    results = data['results']['bindings']
    _results = []
    _i = 0
    _len = results.length
    while _i < _len
      result = results[_i]
      uri = result['tgUris']['value']
      if typeof result['monument'] != 'undefined'
        monument = result['monument']['value']
      else
        monument = " - "
      if typeof result['longcounts'] != 'undefined'
        longcount = result['longcounts']['value']
      else
        longcount = ""
      activityTitle = result['activtiyTitles']['value']
      if typeof result['longcountOfBirths'] != 'undefined'
        longcountBirth = result['longcountOfBirths']['value']
      else
        longcountBirth = ''
      if typeof result['longcountOfDeaths'] != 'undefined'
        longcountDeath = result['longcountOfDeaths']['value']
      else
        longcountDeath = ''
      if typeof result['actorNames'] != 'undefined'
        actorName = result['actorNames']['value']
      else
        actorName = ''
      if typeof result['placeNames'] != 'undefined'
        placeName = result['placeNames']['value']
      else
        placeName = ''
      if typeof result['relatedActivityTitles'] != 'undefined'
        relatedActivityTitle = result['relatedActivityTitles']['value']
      else
        relatedActivityTitle = ''
      if typeof result['agentNames'] != 'undefined'
        agentName = result['agentNames']['value']
      else
        agentName = ''
      button = ""
      if typeof result['sourceDatas'] != 'undefined'
        source = result['sourceDatas']['value']
        sources = source.split('||')
        index = undefined
        sourceList = '<ul>'
        index = 0
        while index < sources.length
          sourceList = sourceList + '<li>' + sources[index] + '</li>'
          ++index
        sourcesList = sourceList + '</ul>'
        uri = uri.replace /:/, "_"
        button = "<button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#" + uri + "\">Show Sources</button>"
      else
        sourcesList = ""
        button = "<button type=\"button\" class=\"btn btn-info\" disabled data-toggle=\"collapse\" data-target=\"#" + uri + "\">Show Sources</button>"

      _results.push $('table#actorActiv').append('<tr><td style="margin-top:100px">' + longcount + '</td><td>' + activityTitle + '</td><td>' + actorName + ' (' + longcountBirth + ' - ' + longcountDeath + ')</td><td>' + placeName + '</td><td>' + relatedActivityTitle + '</td><td>' + agentName + '</td><td>' + monument + '</td><td>' + button + '<div id="' + uri + '" class="collapse">' + sourcesList + '</div></td></tr>')
      _i++
    _results





$(document).on 'ready click', 'li#entityAmount', (e) ->
  $('div#entityAmuont').modal 'show'
  query = queryPrefixes + getEntityAmount()

  request = {
    'query' : query
  }
  $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      cache: false,
      dataType: "json",
      data: request,
      cache: false
  ).done((data) ->
       results = data['results']['bindings']

       for result in results
         if result['gettypes']['value'].indexOf("http://erlangen-crm.org/current/") != -1
           entity = result['gettypes']['value'].substr(result['gettypes']['value'].indexOf("_")+1, result['gettypes']['value'].length)
         else
           if result['gettypes']['value'].indexOf("http://idiom-projekt.de/schema/") != -1
             entity = result['gettypes']['value'].substr("http://idiom-projekt.de/schema/".length, result['gettypes']['value'].length)
           if result['gettypes']['value'].indexOf("http://idiom-projekt.de/catalogue/") != -1
             entity = result['gettypes']['value'].substr("http://idiom-projekt.de/catalogue/".length, result['gettypes']['value'].length)

         count = result['count']['value'].replace /"^^xsd:integer/
         $('table#entityAmuont').append('<tr><td style="margin-top:100px">' + entity + '</td><td>' + count + '</td></tr>')

  )


$(document).on 'ready click', 'input.conedaKorChoice', (e) ->
  id = $(this).val()
  console.log "ID: " + id
  $('div#'+id).find('input[type="checkbox"]').attr('checked',':checked')


#$(document).on 'ready click', 'li#conedakor', (e) ->
processConedaKorQuery = (modalToLoad,pageNumber) ->
    keyword = $('input#' + modalToLoad + '-input').val()
    $('input#conedakorPage').val(pageNumber)
#    $('div#' + modalToLoad + '-results').append('
#    						<table class="blab" id="conedakor">
#    							<tr>
#                    <th style="width:50px" align="center" id="radio">Choose</th>
#                    <th style="width:1000px" align="center" id="image">Image</th>
#                    <th style="width:200px" align="center" id="conedakorid">ConedaKor ID</th>
#                    <th style="width:200px" align="center" id="mediaid">Medium ID</th>
#                    <th style="width:200px" align="center" id="collectionid">Collection ID</th>
#    								<th style="width:100px" align="center" id="name">Name</th>
#    								<th style="width:100px" align="center" id="comment">Comment</th>
#    							</tr>')
    searchurl = "https://classicmayan.org/data/objectAPI/conedaKor/mediaForArtefact/term=" + keyword + "/page=" + pageNumber
    console.log "CONEDAKOR: " + searchurl
    $.ajax(
      type: 'GET',
      url: searchurl,
      #contentType: "application/json; charset=utf-8",
      cache: false,
      dataType:'json'
    ).done((data) ->
      console.log "Bla"
      console.log data
      results = data['result']
      #for result in results
      appendConedaKorResults(results, modalToLoad)
    )

$(document).on 'ready click', 'button#nextConedaKorRes', (e) ->
  $('div#idiom_shows-results').empty()
  #$("table#conedakor").find("tr:gt(0)").remove();
  keyword=$('input#' + modalToLoad + '-input').val()
  pagenumber = $('input#conedakorPage').val()
  pagenumber = parseInt(pagenumber)+1
  console.log("PN: " + pagenumber)
  processConedaKorQuery("idiom_shows",pagenumber)

$(document).on 'ready click', 'button#prevConedaKorRes', (e) ->
  $('div#idiom_shows-results').empty()
  #$("table#conedakor").find("tr:gt(0)").remove();
  keyword=$('input#' + modalToLoad + '-input').val()
  pagenumber = $('input#conedakorPage').val()
  pagenumber = parseInt(pagenumber)-1
  console.log("PN: " + pagenumber)
  processConedaKorQuery("idiom_shows",pagenumber)


processTEIQuery = (modalToLoad) ->
#$(document).on 'ready click', 'button#carries', (e) ->
  console.log "tada"
  $('div#crm_P128_carries').modal 'show'
  keyword = $('input#' + modalToLoad + '-input').val()
  formatToFilter = "format:text/xml"
  projectToFilter = "project.id:TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318"
  parserFileFiler = "notes:PARSER_FILE"
  searchURL = "https://textgridlab.org/1.0/tgsearch/search/?q=" + keyword + "&filter="+ formatToFilter + "&filter=" + projectToFilter + "&sid=" + tgSID + "&limit=3000"
  console.log "TADA: " + searchURL
  $.ajax(
    type: 'GET',
    url: searchURL,
    #contentType: "application/json; charset=utf-8",
    cache: false,
    dataType:'xml'
  ).done((data) ->
      console.log "TADA"
      results = $(data).find('object')
      console.log "RESULTS: " + results
      console.log "SIZE: " + results.length
      console.log data
      appendTEI(results, "crm_P128_carries")
      $(results).each((key, value) ->
        console.log "BLA"
        id = $(value).find("textgridUri").text()
        title = $(value).find("title").text()
        parserFile = $(value).find("notes").text()
        console.log id
        console.log title
        console.log parserFile
      )
  )



$(document).on 'ready click', 'li#graphsWithoutHasIcon', (e) ->
  $('div#graphsWithoutHasIcon').modal 'show'
  query = queryPrefixes + getGraphsWithoutHasIcon()

  request = {
    'query' : query
  }
  $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      cache: false,
      dataType: "json",
      data: request,
      cache: false
  ).done((data) ->
       results = data['results']['bindings']

       for result in results
         textgriduri = result['graph']['value']
         graphNumber = result['graphNumber']['value']
         $('table#graphsWithoutHasIconTable').append('<tr><td style="margin-bottom:10px, width:100px" align="center">' + textgriduri + '</td><td style="width:100px" align="center">' + graphNumber + '</td></tr>')

  )


$(document).on 'ready click', 'li#catalogueConcordanceReverse', (e) ->
  $('div#catalogueConcordanceReverse').modal 'show'

processConcrodaceQuery = (catalogue) ->
  #$('div#catalogueConcordanceReverse table#catalogueConcordanceReverseTable').empty()
  $("table#catalogueConcordanceReverseTable").find("tr:gt(0)").remove();
  console.log "CAT: " + catalogue
  query = queryPrefixes + getCatalogueConcordancePerCatalogue(catalogue)
  console.log "query: " + query
  request = {
    'query' : query
  }
  $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      cache: false,
      dataType: "json",
      data: request,
      cache: false
  ).done((data) ->
       results = data['results']['bindings']

       for result in results
         catalogueTitle = result['catalogueTitles']['value']
         catalogueNumbers = result['catalogueNumbers']['value']
         variants = result['variants']['value']
         IDIOMCatalogue = result['IDIOMCatalogue']['value']
         imageInCatalogue = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + result['imageInCatalogues']['value'] + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
         imageInIDIOMCatalogue = '<div style="padding-top:5px; padding-bottom:5px;"><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + result['imageInIDIOMCatalogues']['value'] + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/>'
         console.log "BLA"
         $('table#catalogueConcordanceReverseTable').append('<tr><td style="margin-bottom:10px, width:100px" align="center">' + catalogueTitle + '</td><td style="width:100px" align="center">' + catalogueNumbers + '</td><td style="width:100px" align="center">' + variants + '</td><td style="width:100px" align="center">' + IDIOMCatalogue + '</td><td style="width:100px" align="center">' + imageInCatalogue + '</td><td style="width:100px" align="center">' + imageInIDIOMCatalogue + '</td></tr>')

  )

$(document).on 'ready click', 'button#openObjectForConedaKor', (e) ->
  e.preventDefault();
  console.log("CONEDAURI");
  setSid($("input#consid").val());
  open($("input#conuri").val());



$(document).on 'ready click', 'button#saveObjectForConedaKor', (e) ->
  e.preventDefault();
  save($("input#conuri").val());

$('div#catalogueConcordanceReverse').on 'ready click', 'ul.dropdown-menu li', ->
  catalogueName = $(this).text()
  $('button#signcatalogue span.value').text(catalogueName)
  processConcrodaceQuery(catalogueName)


generateId = ->
    chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    today = new Date()
    result = today.valueOf().toString 16
    result += chars.substr Math.floor(Math.random() * chars.length), 1
    result += chars.substr Math.floor(Math.random() * chars.length), 1
    return result
