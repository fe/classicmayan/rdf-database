sparqlEndpoint = "https://www.classicmayan.org/trip/metadata/query"

$(document).on("ready click", "li#epigraphicActorReasearchForAll", (e) ->
    e.preventDefault()
    $('div#epigraphicActorSearchQueryForAll').modal 'show'

    request = {
        'query' : queryPrefixes + ' SELECT ?prefActorName ?tgURI WHERE { GRAPH ?tgURI {?tgObject rdf:type idiom:EpigraphicActor. ?tgObject idiom:prefActorAppellation ?prefActorRef. ?prefActorRef idiom:actorName ?prefActorName.}} ORDER BY ASC(?prefActorName)'
    }
    $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      dataType: 'json',
      async: false,
      data: request
    ).done((data) ->
      results = data['results']['bindings']
      for result in  results
        prefActorName = result['prefActorName']['value']
        tgURI = result['tgURI']['value']
        $('ul#actornameForAllTopLevel-results').append('
              <li class="actorNameForResearch">
                <div style="border-bottom:1px groove #ffffff;">' + prefActorName + '<span id="' + tgURI + '" class="glyphicon glyphicon-list-alt actorNameForResearch2"></span>
                  <div id="' + tgURI + '">
				            <div class ="prefActorNames">
					            <form>
                        <div class="prefActornameForAll"></div>
            						<div class="epigraphicActorSearchQueryForAll-results" class="form-gorup">
							            <ul class="actornameForAll-results"></ul>
            						</div>
					            </form>
					            <form>
						            <ul class="sourcesPrefLabelForAll-results"></ul>
					            </form>
      				     </div>
                 </div>
              </li>')
        )
)

$(document).on("ready click", "span.actorNameForResearch2", (e) ->
    graph = $(this).attr('id')
    id = graph.replace /:/, "\\:"
    e.preventDefault()

    request = {
        'query' : queryPrefixes + 'SELECT ?prefActorName ?citation WHERE { GRAPH <' + graph + '> {?tgObject idiom:prefActorAppellation ?prefActorRef. ?prefActorRef idiom:actorName ?prefActorName. OPTIONAL{ ?prefActorRef dct:isReferencedBy ?sourceRef.
    ?sourceRef dct:bibliographicCitation ?citation. }}}'
    }
    prefActorName = ""
    prefActorCitations = []
    prefActorCitations2 = []
    altActorRefs = []
    alternativeCitation = []

    #GET ACTOR PREF NAME AND SOURCES

    $.ajax(
        type: 'GET',
        url: sparqlEndpoint,
        cache: false,
        dataType: "json",
        async: false,
        data: request
    ).done((data) ->
        results = data['results']['bindings']
        for result in results
          prefActorName = result['prefActorName']['value']
          if typeof result['citation'] == "undefined"
            prefActorCitations = ""
          else
            prefActorCitations.push result['citation']['value']


        $('div#' + id + ' div.prefActornameForAll').append('<strong>' + prefActorName + ' (Prefered Name)</strong>')


        if prefActorCitations.length == 0
          $('div#' + id + ' ul.sourcesPrefLabelForAll-results').append('<li>No Literature entered</li>')
        else
          for source in prefActorCitations
            $('div#' + id + ' ul.sourcesPrefLabelForAll-results').append('<li>' + source + '</li>')

        #GET ALTERNATIVE ACTOR REFS

        request = {
          'query' : queryPrefixes + 'SELECT DISTINCT ?altActorRef WHERE { GRAPH <' + graph  + '> {?tgObject idiom:altActorAppellation ?altActorRef.}}'
        }
        $.ajax(
          type: 'GET',
          url: sparqlEndpoint,
          dataType: "json",
          async: false,
          data: request
        ).done((data) ->
            results = data['results']['bindings']
            for result in results
              altActorRef = result['altActorRef']['value'].substring("http://idiom-projekt.de/schema/".length, result['altActorRef']['value'].length)
              $('div#' + id + ' div.prefActorNames').after().append('<div id="' + altActorRef + '"><strong></strong><ul id="' + altActorRef + '"></div><br/>')
              altActorRefs.push altActorRef
        )

    )
    for actorRefUri in altActorRefs
          request = {
                      'query' : queryPrefixes + 'SELECT ?citation ?altActorName WHERE { GRAPH <' + graph + '> {idiom:' + actorRefUri + '  dct:isReferencedBy ?sourceRef. ?sourceRef dct:bibliographicCitation ?citation. idiom:' + actorRefUri +  ' idiom:actorName ?altActorName. }}'
          }
          $.ajax(
              type: 'GET',
              url: sparqlEndpoint,
              dataType: 'json',
              async: false,
              data: request
          ).done((data) ->
              results = data['results']['bindings']
              for result in results
                if($('div#' + actorRefUri).text() < 1)
                  $("div#" + actorRefUri + " strong").append(result['altActorName']['value'])
                alternativeCitation.push result['citation']['value']
              for cit in  alternativeCitation
                $('ul#' + actorRefUri).append('<li>' + cit + '</li>')
              alternativeCitation = []
          )

)

$(document).on("ready click", "li#concordanceInCatalogueEntries", (e) ->
    e.preventDefault()
    $('table#concordanceQuery').empty()
    $('div#concordanceQuery').modal 'show'
    request = {
        'query' : queryPrefixes + ' SELECT
  (group_concat( distinct ?tgObject;separator="; ") as ?tgObjects)
  (group_concat( distinct ?graph;separator="; ") as ?graphs)
  (group_concat( distinct ?variationType;separator="; ") as ?variationTypes)
  (group_concat( distinct ?digitalImageOfGraph;separator="; ") as ?digitalImageOfGraphs)
  (group_concat( distinct ?catalogue;separator="; ") as ?catalogues)
  (group_concat( distinct ?catalogueNumber;separator="; ") as ?catalogueNumbers)
  (group_concat( distinct ?image;separator="; ") as ?images)
    WHERE {
      GRAPH ?textgridURI {
        ?tgObject rdf:type idiomcat:Graph.
    	?tgObject idiomcat:graphNumber ?graphNumber.
    	?tgObject idiomcat:hasDigitalImage ?digitalImageOfGraph.
    	?tgObject idiomcat:hasCatalogueEntry ?catalogueEntryRef.
        ?tgObject idiomcat:variationType ?variationType.
        BIND(REPLACE(STR(?graphNumber), ?variationType,"") AS ?graph) .
    	BIND(IRI(CONCAT(' + "<" + ',?catalogueEntryRef,' + ">" + ')) AS ?catalogueEntryURL)
    	GRAPH ?catalogueEntryURL {
      		?catalogueEntryRef idiomcat:titleOfCatalogue ?catalogue.
      		?catalogueEntryRef idiomcat:catalogueNumber ?catalogueNumber.
      		?catalogueEntryRef idiomcat:imageInCatalogue ?image.
    	}
    }
} GROUP BY ?graphNumber ?catalogueNumber ORDER BY ASC(xsd:integer(?graphs))'
    }
    $.ajax(
      type: 'GET',
      url: sparqlEndpoint,
      dataType: 'json',
      async: false,
      data: request
    ).done((data) ->
      results = data['results']['bindings']
      for result in  results
        graphNumber = result['graphs']['value']
        variationType = result['variationTypes']['value']
        tgURI = result['tgObjects']['value']
        digitalImageOfGraph = result['digitalImageOfGraphs']['value']
        catalogue = result['catalogues']['value']
        catalogueNumber = result['catalogueNumbers']['value']
        image = result['images']['value']
        images=[]
        imagesToDisplay=""
        images = image.split ";"

        for image in images
          image = image.replace /^\s+/g, ""
          image="<img src=\"https://textgridlab.org/1.0/digilib/rest/IIIF/" + image + ';sid=' + tgSID + '/full/,40/0/native.jpg\" alt=\"\"/>'
          imagesToDisplay = imagesToDisplay.concat(image)
        $('table#concordanceQueryTable').append('<tr><td>' + graphNumber+ '</td><td>' + variationType + '</td><td><img src="https://textgridlab.org/1.0/digilib/rest/IIIF/' + digitalImageOfGraph + ';sid=' + tgSID + '/full/,40/0/native.jpg" alt=""/></td><td>' + catalogue+ '</td><td>' + catalogueNumber+ '</td><td>' + imagesToDisplay + '</td></tr>')
        )
)
