appendActor = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
        id = result['tgObjects']['value']
        actorName = result['actorNames']['value']

        if (typeof result['pndIDs'] == "undefined")
           pndID = "No PND-ID given "
        else
           pndID = result['pndIDs']['value']

        if (typeof result['genders'] == "undefined")
           gender = "No gender given "
        else
           gender = result['genders']['value']

        if (typeof result['birthDates'] == "undefined")
           birthDate = "No birthDates given "
        else
           birthDate = result['birthDates']['value']

        if (typeof result['deathDates'] == "undefined")
           deathDate = "No deathDate given "
        else
           deathDate = result['deathDates']['value']

        $('div#' + modalToLoad + '-results').append(
            '<div class="radio">
                <label>
                    <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
                        <strong>' + actorName +  ' (' + gender + ') ' + ' : ' + '</strong>' + pndID + ', <strong>Birth Date:</strong> ' + birthDate + ', <strong>Death Date:  </strong>' + deathDate +  '</strong>
                    </label>
                </div>'
        )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendMisc = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
        id = result['tgObjects']['value']
        actorName = result['actorName']['value']

        if(typeof result['id'] == "undefined")
           furtherID = "No further identifier"
        else
           furtherID = result['id']['value']

        if(typeof result['residenceName'] == "undefined")
           residenceName = "No residence given"
        else
           residenceName = result['residenceName']['value']

        if(typeof result['details'] == "undefined")
           details = "No details given"
        else
           details = result['details']['value']

        $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + actorName +  ' : ' + '</strong>' + ", Other Identifier: " + furtherID + ', Residence: ' + residenceName + ', Details: ' + details + '
            </label>
          </div>'
        )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendArtefact = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
    for result in results

      id = result['tgObjects']['value']

      if(typeof result['prefTitles'] == "undefined")
           prefTitle = "No Title given, but this shouldn't be"
      else
           prefTitle = result['prefTitles']['value']

      if(typeof result['titleDisps'] == "undefined")
           titleDisp = "No Title given"
      else
           titleDisp = result['titleDisps']['value']

      if(typeof result['titleTypes'] == "undefined")
           titleType = "No Title Type given"
      else
           titleType = result['titleTypes']['value']

      if(typeof result['idiomNumbers'] == "undefined")
           idiomNumber = "No IDIOM number given"
      else
           idiomNumber = result['idiomNumbers']['value']

      if(typeof result['addIdentifierNames'] == "undefined")
           addIdentifierName = "No additional identifier given"
      else
           addIdentifierName = result['addIdentifierNames']['value']

      if(typeof result['addIdentifierTypes'] == "undefined")
           addIdentifierType = "No additional identifier given"
      else
           addIdentifierType = result['addIdentifierTypes']['value']

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>'  + prefTitle + ": " + '</strong>' + ' Additional Title: ' + titleDisp + ' ( Title Type: '  + titleType + ')' + ', IDIOM-Number: ' + idiomNumber + ', Additional Identifier: ' + addIdentifierName + ' ( Identifier Type: ' + addIdentifierType + ') ' + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendNonEpigraphic = (results, modalToLoad) ->

    for result in results
      if (typeof result['tgObjects'] != "undefined")
        if ($('.resultLength').length == 0)
          $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
        actorName = result['actorNames']['value']
        id = result['tgObjects']['value']
        objectType = result['types']['value']

        if (typeof result['lifedatas'] == "undefined")
             lifedata = "No Lifedata given"
        else
             lifedata = result['lifedatas']['value']

        if (typeof result['pndIDs'] == "undefined")
             pndID = "No PND reference gicen"
        else
             pndID = result['pndIDs']['value']

        if (typeof result['memberships'] == "undefined")
             membership = "Not Member of Any Group"
        else
             membership = result['memberships']['value']

        if (typeof result['detailss'] == "undefined")
             details = "Not Member of Any Group"
        else
             details = result['detailss']['value']

        ## TODO: Replace this with the Group Type given within the vocabulary
        if (typeof result['detailss'] == "undefined")
             details = "Group Type"
        else
             details = result['detailss']['value']

        if(objectType.indexOf("NonEpigraphicGroup") > 0)
          objectType = "NonEpigraphicGroup"
        if(objectType.indexOf("NonEpigraphicPerson") > 0)
          objectType = "NonEpigraphicPerson"

        $('div#'  + modalToLoad +  '-results').append(
            '<div class="radio">
              <label>
                <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
                <strong>' + actorName + " (" + lifedata + ") " + " : " + '</strong>' + details + ", " + objectType + ", pndID: " + pndID + ", " + "Membership: " + membership + '</strong>
              </label>
            </div>'
        )

        $("button#" + modalToLoad + "Search").prop('disabled', false)
        $("button#" + modalToLoad + "Search").text('Search')

        $('button#set-' + modalToLoad).click((e) ->
          e.preventDefault()
          setTheId(modalToLoad, id)
        )
      else
        $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">0 Matches </h3>')
        $("button#" + modalToLoad + "Search").prop('disabled', false)
        $("button#" + modalToLoad + "Search").text('Search')


appendEpigraphic = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      actorName = result['actorName']['value']
      id = result['tgObjects']['value']

      if (typeof result['details'] == "undefined")
           pndID = " ? "
      else
           pndID = result['details']['value']

      if (typeof result['details'] == "undefined")
           details = "Group Type"
      else
           details = result['details']['value']

      lifedata = result['lifedata']['value']
      objectType = result['type']['value']

      if(objectType.indexOf("EpigraphicGroup") > 0)
        objectType = "EpigraphicGroup"
      if(objectType.indexOf("EpigraphicActor") > 0)
        objectType = "EpigraphicPerson"

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + actorName + " (" + lifedata + ") " + " : " + '</strong>' + details + ", " + objectType + ", pndID: " + pndID + ", " + '</strong>
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendAllActor = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
    for result in results
      actorName = result['actorName']['value']
      id = result['tgObjects']['value']

      if (typeof result['pndID'] == "undefined")
           pndID = "No pndID given"
      else
           pndID = result['pndID']['value']

      if (typeof result['details'] == "undefined")
           details = "Group Type"
      else
           details = result['details']['value']

      if (typeof result['lifedata'] == "undefined")
           lifedata = "No Lifedata given"
      else
           lifedata = result['lifedata']['value']

      if (typeof result['type'] == "undefined")
           objectType = "No objectType given"
      else
           objectType = result['type']['value']

      if(objectType.indexOf("NonEpigraphicGroup") > 0)
        objectType = "NonEpigraphicGroup"
      if(objectType.indexOf("EpigraphicGroup") > 0)
        objectType = "EpigraphicGroup"
      if(objectType.indexOf("EpigraphicActor") > 0)
        objectType = "EpigraphicActor"
      if(objectType.indexOf("NonEpigraphicPerson") > 0)
        objectType = "NonEpigraphicPerson"

       $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + actorName + " (" + lifedata + ") " + " : " + '</strong>' + " Object Type: " + objectType + ", Detail: " + details + ", pndID: " + pndID + '</strong>
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendPlace = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
    for result in results
      id = result['tgObjects']['value']
      placeName = result['placeNames']['value']

      if (typeof result['latitudes'] == "undefined")
           latitude = " ? "
      else
           latitude = result['latitudes']['value']

      if (typeof result['longitudes'] == "undefined")
           longitude = " ? "
      else
           longitude = result['longitudes']['value']

      if (typeof result['altitudes'] == "undefined")
           altitude = " ? "
      else
           altitude = result['altitudes']['value']

      if (typeof result['archeologicalCoordinatess'] == "undefined")
           archeologicalCoordinates = "No Coordinates"
      else
           archeologicalCoordinates = result['archeologicalCoordinatess']['value']

      if (typeof result['locatedPlaceName'] == "undefined")
           locatedPlaceName = "Not located Place Name"
      else
           locatedPlaceName = result['locatedPlaceName']['value']


      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + placeName + ' (' + locatedPlaceName + ')</strong>' + ' : ' + 'Latitude: ' + latitude + ', Longitude: ' + longitude + ', Altitude: ' + altitude + ', Archeological Coordinates: ' + archeologicalCoordinates + ", Located Within: " + locatedPlaceName + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendDiscovery = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      title = result['title']['value']

      if(typeof result['placeName'] == "undefined")
           placeName = "No Place Reference given"
      else
           placeName = result['placeName']['value']

      if(typeof result['explorerFamilyName'] == "undefined")
           explorerFamilyName = "No Explorer Ref given"
      else
           explorerFamilyName = result['explorerFamilyName']['value']

      if(typeof result['explorerGivenName'] == "undefined")
           explorerGivenName = "No Explorer given"
      else
           explorerGivenName = result['explorerGivenName']['value']


      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + title + '</strong>' + ': ' + ' Place: ' + placeName + ', Explorer: ' + explorerGivenName + ' ' + explorerFamilyName + '
            </label>
          </div>'
      )

     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendActivity = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']

      if (typeof result['types'] == "undefined")
           type = "not assigned"
      else
           type = result['types']['value']

      activityTitle = result['activityTitles']['value']

      if (typeof result['gregorianTimeSpans'] == "undefined")
           gregorianTimeSpan = "No Gregorian Time Span given"
      else
           gregorianTimeSpan = result['gregorianTimeSpans']['value']

      if (typeof result['placeNames'] == "undefined")
           placeName = "No Place given"
      else
           placeName = result['placeNames']['value']


       $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
               <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + activityTitle + ', ' +  ' (' + type + ')' + '</strong>' + ': ' + "Place: " + placeName + ', Gregorian Span: ' + gregorianTimeSpan + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendCustody = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      custodyTitle = result['custodyTitles']['value']

      if(typeof result['custodyCollections'] == "undefined")
          custodyCollection = " ? "
      else
          custodyCollection = result['custodyCollections']['value']

      if(typeof result['custodians'] == "undefined")
           custodian = "Not Assigned"
      else
           custodian = result['custodians']['value']

      if(typeof result['custodyBegins'] == "undefined")
           custodyBegin = "Not Assigned"
      else
           custodyBegin = result['custodyBegins']['value']

      if(typeof result['custodyEnds'] == "undefined")
           custodyEnd = "Not Assigned"
      else
           custodyEnd = result['custodyEnds']['value']

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + custodyTitle + ': ' + '</strong>' + ' : Collection:' + custodyCollection + ', Begin: ' + custodyBegin + ', End: ' + custodyEnd + ', Custodian: ' + custodian + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendProduction = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      activityTitle = result['activityTitle']['value']

      if (typeof result['productionBeginLC'] == "undefined")
           productionBeginLC = " ? "
      else
           productionBeginLC = result['productionBeginLC']['value']

      if (typeof result['productionEndLC'] == "undefined")
           productionEndLC = " ? "
      else
           productionEndLC = result['productionEndLC']['value']

      if (typeof result['productionDateExactlyLC'] == "undefined")
           productionDateExactlyLC = " ? "
      else
           productionDateExactlyLC = result['productionDateExactlyLC']['value']

      if (typeof result['qualifier'] == "undefined")
           qualifier = " ? "
           date = " ? "
      else
           qualifier = result['qualifier']['value']

           if((qualifier == "Begin" || "End") && (typeof result['productionBeginLC'] != "undefined"))
                date = result['productionBeginLC']['value'] + " - " + result['productionEndLC']['value']
           else if qualifier == "Exactly"
                date = result['productionDateExactlyLC']['value']
           else if qualifier == " ? "
                date = " ? "
           else
                date = " ? "

      if (typeof result['placeName'] == "undefined")
           placeName = "No Place given"
      else
           placeName = result['placeName']['value']

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + activityTitle + ' (' +  placeName + ')'+ '</strong>' + ' : ' + " , Long Count: " + date + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )


appendDedication = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['dedication']['value']
      dedicationTitle = result['title']['value']


      if (typeof result['longcount'] == "undefined")
        date = "no date given"
      else
        date = result['longcount']['value']

      if (typeof result['placeName'] == "undefined")
        placeName = "no place given"
      else
        placeName = result['placeName']['value']


      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + dedicationTitle + ' (' +  placeName + ')'+ '</strong>' + ' : ' + " , Longcount: " + date + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )


appendGroup = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      groupName = result['groupNames']['value']

      if (typeof result['nonEpigraphicGroupAlternativeNames'] == "undefined")
        nonEpigraphicGroupAlternativeName = "Not Assigned"
      else
        nonEpigraphicGroupAlternativeName = result['nonEpigraphicGroupAlternativeNames']['value']

      if (typeof result['gndRefs'] == "undefined")
        gndRef = "No GND Reference given"
      else
        gndRef = result['gndRefs']['value']

      if (typeof result['groupTypes'] == "undefined")
        groupType = "No Group Type given"
      else
        groupType = result['groupTypes']['value']

      if (typeof result['residenceTitles'] == "undefined")
        residenceTitle = "No residence given"
      else
        residenceTitle = result['residenceTitles']['value']

      if (typeof result['residenceTitleTypes'] == "undefined")
        residenceTitleType = "No residence title type given"
      else
        residenceTitleType = result['residenceTitleTypes']['value']

      if (typeof result['types'] == "undefined")
        type = " ? "
      else
        type = result['types']['value']


      if (typeof result['groupRefs'] == "undefined")
           groupRef = "Group is not member of another group"
      else
           groupRef = result['groupRefs']['value']

      typePrefix = "file:///home/mbrodhun/mayandatabase/"
      type = type.substring(typePrefix.length, type.length)

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + groupName + ', ' +  ' ('  + type + ')' + '</strong>' + ' Alternative Name: ' + nonEpigraphicGroupAlternativeName + ', GND-ID: ' + gndRef + ', Group-Type: ' + groupType + ', Residence Title: ' + residenceTitle + ', Residence Title Type: ' + residenceTitleType + ', Group is Member of Group ' + groupRef + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendAllActivities = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      objectType = result['type']['value']
      activityTitle = result['activityTitle']['value']

      if (typeof result['gregorianDate'] == "undefined")
           gregorianDate = "No Gregorian Time Span given"
      else
           gregorianDate = result['gregorianDate']['value']

      if (typeof result['lc'] == "undefined")
           longcount = "?"
      else
           longcount = result['lc']['value']

      if(objectType.indexOf("E7_Activity") > 0)
        objectType = "Activity"
      if(objectType.indexOf("Acquisition") > 0)
        objectType = "Acquisition"
      if(objectType.indexOf("Custody") > 0)
        objectType = "Custody"
      if(objectType.indexOf("Discovery") > 0)
        objectType = "Discovery"
      if(objectType.indexOf("Production") > 0)
        objectType = "Production"
      if(objectType.indexOf("E80_Part_Removal") > 0)
        objectType = "PartRemoval"

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + activityTitle + ' (' +  objectType + ')'+ '</strong>' + ' : ' + longcount + " (" + gregorianDate + ")" +  '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendPartRemoval = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      title = result['title']['value']

      if (typeof result['relatedActivityTitle'] == "undefined")
           relatedActivityTitle = "No related activity title"
      else
           relatedActivityTitle = result['relatedActivityTitle']['value']

      if (typeof result['date'] == "undefined")
           date = "No Date given"
      else
           date = result['date']['value']

      if (typeof result['placeName'] == "undefined")
           placeName = "No Place given"
      else
           placeName = result['placeName']['value']

      if (typeof result['relatedActivityTitle'] == "undefined")
           relatedActivityTitle = "Not Assigned"
      else
           relatedActivityTitle = result['relatedActivityTitle']['value']

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + title + '</strong>' + ' : Date: ' + date  + " Place: " + placeName + ', Realated Activity: ' +  relatedActivityTitle + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendVoc = (results, modalToLoad) ->

    for result in results

      id = result['topConceptUris']['value']

      if (typeof result['topConceptNames'] == "undefined")
           conceptName = " "
      else
           conceptName = result['topConceptNames']['value']

      if (typeof result['nexts'] == "undefined")
           next = " "
      else
           next = result['nexts']['value']

      if (typeof result['defs'] == "undefined")
           definition = " "
      else
           definition = result['defs']['value']


      expand = '<span class="glyphicon glyphicon-circle-arrow-right topConcept"></span>'
      if !id.match(/^http:\/\/localhost/)
      	labelid = id.replace("http://idiom-projekt.de/voc/", "")
      else
        labelid = id.replace("http://localhost:8080/SkosVocabularyEditor/UserDefinedConcepts#", "")

      labelid = labelid.replace("/", "_")

      if typeof result['nexts'] != "undefined"
        label = '<label title="' + definition + '" class="topConcept" id="' + labelid + '"><input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">' + conceptName + ' ' + expand + ' </label>'
      else
        label = '<label title="' + definition + '" class="topConcept" id="' + labelid + '"><input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">' + conceptName + ' </label>'

      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">' + label + '</div>'
      )

      $('span.topConcept').unbind()
      $('span.topConcept').click((e) ->

            vocabid =  $(this).parent().attr('id')
            layerDepth = $(this).parents("label.topConcept").length
            $(this).after('<div class="vocabularyResultLayer' + layerDepth + '"></div>')
            getVocabluarySecondConcept(vocabid, layerDepth)
            $(this).remove()
      )

     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendVisualDocument = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']

      if (typeof result['titleDisp'] == "undefined")
           titleDisp = " ? "
      else
           titleDisp = result['titleDisp']['value']


      if (typeof result['rights'] == "undefined")
           rights = " ? "
      else
           rights = result['rights']['value']


      if (typeof result['creator'] == "undefined")
           creator = " ? "
      else
           creator = result['creator']['value']

      if (typeof result['inventoryNumber'] == "undefined")
           inventoryNumber = " ? "
      else
           inventoryNumber = result['inventoryNumber']['value']


      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
               <strong>' + titleDisp + '</strong>' + ' : Rights: ' + rights  + "Creator: " + creator + ', Inventory Number: ' +  inventoryNumber + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendEpigraphicActor = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      if typeof result['tgObjects'] != 'undefined'

        id = result['tgObjects']['value']
        actorName = result['actorNames']['value']

        if (typeof result['pndIDs'] == "undefined")
             pndID = "No PND-ID given "
        else
             pndID = result['pndIDs']['value']

        if (typeof result['genders'] == "undefined")
             gender = "No gender given ? "
        else
             gender = result['genders']['value']

        if (typeof result['birthDates'] == "undefined")
             birthDate = "No birthDates given "
        else
             birthDate = result['birthDates']['value']

        if (typeof result['deathDates'] == "undefined")
             deathDate = "No deathDate given "
        else
             deathDate = result['deathDates']['value']


        $('div#'  + modalToLoad +  '-results').append(
            '<div class="radio">
              <label>
                <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
                <strong>' + actorName +  ' (' + gender + ') ' + ' : ' + '</strong>' + pndID + ', Birth Date: ' + birthDate + ', Death Date:  ' + deathDate + '
              </label>
            </div>'
        )
       $("button#" + modalToLoad + "Search").prop('disabled', false)
       $("button#" + modalToLoad + "Search").text('Search')

       $('button#set-' + modalToLoad).click((e) ->
          e.preventDefault()
          setTheId(modalToLoad, id)
       )


appendAcquisition = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')

    for result in results
      id = result['tgObjects']['value']
      activityTitle = result['activityTitle']['value']

      if (typeof result['timeSpan'] == "undefined")
           timeSpan = " ? "
      else
           timeSpan = result['timeSpan']['value']

      if (typeof result['from'] == "undefined")
           from = " ? "
      else
           from = result['from']['value']

      if (typeof result['to'] == "undefined")
           to = " ? "
      else
           to = result['to']['value']



      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + activityTitle +  ' : ' + '</strong>' + timeSpan + ', From: ' + from + ', To:  ' + to + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )

appendSign = (results, modalToLoad) ->

    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
    for result in results
      id = result['tgObjects']['value']
      signNumber = result['signNumber']['value']
      diacriticFunction = " Diacritic Function: "
      logographicReading = " Logographic Reading: "
      logographicMeaning = " Logographic Meaning: "
      syllabicReading = " Syllabic Reading: "
      numericFunction = " Numeric Function: "

      if (typeof result['cldfValue'] == "undefined")
           cldfValue = ""
      else
          cldfValue = " Confidence Level: " + result['cldfValue']['value']
          diacriticFunction = diacriticFunction + cldfValue + " |"

      if (typeof result['transValueDF'] == "undefined")
           transValueDF = ""
      else
           transValueDF = result['transValueDF']['value']
           diacriticFunction = diacriticFunction + "| " + transValueDF  + " "


      if (typeof result['cllmValue'] == "undefined")
           cllmValue = ""
      else
           cllmValue = " Confidence Level: " + result['cllmValue']['value']
           logographicMeaning = logographicMeaning + cllmValue + " |"

      if (typeof result['transValueLM'] == "undefined")
           transValueLM = ""
      else
           transValueLM = result['transValueLM']['value']
           logographicMeaning = logographicMeaning + "| " + transValueLM + " "



      if (typeof result['cllrValue'] == "undefined")
           cllrValue = ""
      else
           cllrValue = " Confidence Level: " + result['cllrValue']['value']
           logographicReading = logographicReading + cllrValue + " |"

      if (typeof result['transValueLR'] == "undefined")
           transValueLR = ""
      else
           transValueLR = result['transValueLR']['value']
           logographicReading = logographicReading + "| " + transValueLR + " "


      if (typeof result['clsrValue'] == "undefined")
           clsrValue = ""
      else
           clsrValue = " Confidence Level: " + result['clsrValue']['value']
           syllabicReading = syllabicReading + clsrValue + " |"

      if (typeof result['transValueSR'] == "undefined")
           transValueSR = ""
      else
           transValueSR = result['transValueSR']['value']
           syllabicReading = syllabicReading + "| " + transValueSR + " "

      if (typeof result['clnfValue'] == "undefined")
           clnfValue = ""
      else
           clnfValue = " Confidence Level: " + result['clnfValue']['value']
           numericFunction = numericFunction + clnfValue + " |"

      if (typeof result['transValueNF'] == "undefined")
           transValueNF = ""
      else
           transValueNF = result['transValueNF']['value']
           numericFunction = numericFunction + "| " + transValueNF + " "


      if syllabicReading.length == " Syllabic Reading: ".length
        syllabicReading = ""

      if logographicMeaning.length == " Logographic Meaning: ".length
        logographicMeaning = ""

      if logographicReading.length == " Logographic Reading: ".length
        logographicReading = ""

      if diacriticFunction.length == " Diacritic Function: ".length
        diacriticFunction = ""

      if numericFunction.length == " Numeric Function: ".length
        numericFunction = ""


      $('div#'  + modalToLoad +  '-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
              <strong>' + signNumber +  ' : ' + '</strong>' + diacriticFunction + logographicMeaning + logographicReading + syllabicReading + numericFunction + '
            </label>
          </div>'
      )
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')

     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setTheId(modalToLoad, id)
     )


appendGraph = (results, modalToLoad) ->
    $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
    console.log(modalToLoad)
    for result in results
      id = result['tgObjects']['value']

      if (typeof result['catalogueDatas'] == "undefined")
           catalogueData = " ? "
      else
           catalogueData = result['catalogueDatas']['value']

      if (typeof result['graphNumbers'] == "undefined")
           graphNumber = " ? "
      else
           graphNumber = result['graphNumbers']['value']

      if (typeof result['nickNames'] == "undefined")
           nickName = " ? "
      else
           nickName = result['nickNames']['value']



      $('div#' + modalToLoad + '-results').append('<div><input type="checkbox" name="' + modalToLoad + '-radios" value="' + id + '"><strong> Graph Number: </strong>' + graphNumber + ', ' + 'Nicknames: ' + nickName + ' Catalogue Entry: ' + catalogueData + '</label></div>')
      #$('div#'  + modalToLoad +  '-results').append(
      #    '<div class="radio">
      #      <label>
      #        <input type="radio" name="' + modalToLoad + '-radios" value="' + id + '">
      #        <strong> Graph Number: </strong>' + graphNumber + ', ' + 'Nicknames: ' + nickName + ' Catalogue Entry: '  + catalogueData +  '
      #      </label>
      #    </div>'
      #)
     $("button#" + modalToLoad + "Search").prop('disabled', false)
     $("button#" + modalToLoad + "Search").text('Search')
     labelText = "";
     if (modalToLoad == "idiomcat_isGraphOf")
       labelText = "is Graph of Sign"
     if (modalToLoad == "idiomcat_isDerivedFrom")
       labelText = "is derived from Graph"
     if (modalToLoad == "idiomcat_isDividedInto")
       labelText = "is devided into Graph"
     if (modalToLoad == "idiomcat_isMergedInto")
       labelText = "is integrated into Graph"
     if (modalToLoad == "idiomcat_containsIconicElementsFrom")
       labelText = "contains iconic elements from Graph"
     if (modalToLoad == "idiomcat_sharesDiagnosticElementsWith")
       labelText = "shares Diagnostic Elements with Graph"
     $('button#set-' + modalToLoad).click((e) ->
        e.preventDefault()
        setMultipleIDs(modalToLoad, $('input[type="checkbox"]:checked').val(), labelText);
        #setTheId(modalToLoad, id)
     )


appendTEI = (results, modalToLoad) ->

  $('div#' + modalToLoad + ' .modal-header').append('<h3 class="resultLength">' + results.length + ' Matches </h3>')
  $(results).each((key, value) ->
      console.log "APENDIX"
      id = $(value).find("textgridUri").text()
      title = $(value).find("title").text()
      parserFile = $(value).find("notes").text()
      if(parserFile == "PARSER_FILE")
        $('div#crm_P128_carries-results').append(
                  '<div class="radio">
                    <label>
                      <input type="radio" name="crm_P128_carries-radios" value="' + id + '">
                      <strong>' + title + '</strong>
                    </label>
                  </div>'
        )
      $("button#" + modalToLoad + "Search").prop('disabled', false)
      $("button#" + modalToLoad + "Search").text('Search')
      $('button#set-' + modalToLoad).click((e) ->
          e.preventDefault()
          console.log "MODAL_TO_LOAD: " + modalToLoad
          setTheId(modalToLoad, id)
      )
  )

appenArtefactAmount = () ->
  keyword = $('input#listAllArtefactsAlphabetical-input').val()
  request = {'query' : queryPrefixes + getArtefactAmount(keyword)}
  uppercount = 0
  $.ajax(
    type: 'GET',
    url: sparqlEndpoint,
    cache: false,
    dataType: "json",
    async: false,
    data: request
  ).done((data) ->
    results = data['results']['bindings']

    for result in results
      count = result['count']['value']
      JSON.stringify(count)
      if ($('.resultLength').length == 0)
        $('div#listAllArtefactsAlphabetical .modal-header').append('<h3 class="resultLength">' + count + ' Matches </h3>')
      else
        $("h3.resultLength").text(count + " Matches")
  )


appendArtefactList = (results) ->
  keyword = $('input#listAllArtefactsAlphabetical-input').val()
  size = appenArtefactAmount(keyword)
  #console.log "NOT stringified: " + size.currency[0].amount

  #if ($('.resultLength').length == 0)
    #conosle.log "hatschi"
    #$('div#listAllArtefactsAlphabetical .modal-header').append('<h3 class="resultLength">' + size + ' Matches </h3>')
  for result in results
    if (typeof result['prefTitles'] == "undefined")
      title = " ? "
    else
      title = result['prefTitles']['value']

    if (typeof result['addIdentifierNames'] == "undefined")
      addIdentifierNames = " ? "
    else
      addIdentifierNames = result['addIdentifierNames']['value']

    if (typeof result['addIdentifierTypes'] == "undefined")
      addIdentifierTypes = " ? "
    else
      addIdentifierTypes = result['addIdentifierTypes']['value']

    idiomNumber = result['idiomNumbers']['value']
    console.log title + " " + addIdentifierNames + " " + addIdentifierTypes
    $('table#artefactTable').append('<tr><td style="margin-top:100px">' + title + '</td><td>' + addIdentifierNames + '</td><td>' + addIdentifierTypes + '</td><td>' + idiomNumber + '</td></tr>')


appendConedaKorResults= (results, modalToLoad) ->
  id=""
  for result in results
    id = result['id']
    display_name = result['display_name']
    #$('div#idiom_shows-results').append('<div><div id=\"' + id + '\" class=\"imageBox\"></div></div>')

    $('div#' + modalToLoad + '-results').append(
        '<div class="radio">
            <label>
                <h2>' + display_name + '</h2>
                <div><div id=\"' + id + '\" class=\"imageBox\"><input class="conedaKorChoice" type="radio" name="' + modalToLoad + '-radios" value="' + id + '"></div></div>
            </label>
        </div>'
    )


    for media in result["media"]
      previewversion = media.link.replace /original/, "preview"
      console.log "LINK: " + media.link
      console.log "MEDIA_ID: " + media.medium_id

      $('div#idiom_shows-results div#'+id).append('<input type="checkbox" name="' + modalToLoad + '-radios" value="' + media.medium_id + '"><img src="https://classicmayan.kor.de.dariah.eu' + previewversion +  ' alt="" style="margin:5px" align="left">')


  $("button#" + modalToLoad + "Search").prop('disabled', false)
  $("button#" + modalToLoad + "Search").text('Search')
  $('button#set-' + modalToLoad).click((e) ->
    e.preventDefault()

    console.log "HALLO " + $('input[type="checkbox"]:checked').val()
    setMultipleIDs(modalToLoad, $('input[type="checkbox"]:checked').val(), "Image in Maya Image Database")
  )

  #  id = result['id']
  #  if (typeof result.medium == "undefined")
  #    media = "X"
  #  else
  #    media = result.medium.url.thumbnail

  #  if (typeof result.medium_id == "undefined")
  #    mediumID = "null"
    #else
    #  mediumID = result.medium_id
    #collectionID = result['collection_id']
    #kindId = result['kind_id']
    #creatorId = result['creator_id']
    #kindName = result['kind_name']
    #name = result.dataset.image_description
    #distinctName = result['distinct_name']
    #displayName = result['display_name']
    #comment = result['comment']
    #subtype = result['subtype']



    #$('table#conedakor').append('<tr style="height:150px">
    #  <td align="center"><input type="checkbox" name="' + modalToLoad + '-radios" value="' + id + '"></td>
  #    <td align="center"><img src="https://classicmayan.kor.de.dariah.eu/' + media +  ' alt="Image in ConedaKor Database""></td>
  #    <td align="center">' + id + '</td>
#      <td align="center">' + collectionID + '</td>
#      <td align="center">' + name + '</td>
#      <td align="center">' + comment + '</td>
#
#      </tr>')
