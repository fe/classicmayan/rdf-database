package com.github.hriebl.bdnDatabase;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.DatasetAccessor;
import org.apache.jena.query.DatasetAccessorFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

/**
 *
 */
class FusekiCRUD {

  DatasetAccessor accessor;
  List<String> artefactUriList = new ArrayList<String>();
  List<String> allObjectUriList = new ArrayList<String>();
  List<String> allUrisWithMissingCitation = new ArrayList<String>();
  List<String> wrongZuriList = new ArrayList<String>();

  String prefixes =
      "prefix dct: <http://purl.org/dc/terms#>\n" +
          "prefix dc: <http://purl.org/dc/elements/1.1/>\n" +
          "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
          "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
          "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
          "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
          "prefix idiom: <http://idiom-projekt.de/schema/>\n" +
          "prefix ecrm: <http://erlangen-crm.org/current#>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm#>\n" +
          "prefix form: <http://rdd.sub.uni-goettingen.de/rdfform#>\n" +
          "prefix crm: <http://erlangen-crm.org/current/>\n" +
          "prefix tgforms: <http://www.tgforms.de/terms#>\n" +
          "prefix textgrid: <http://textgridrep.de/>\n" +
          "prefix schema: <http://schema.org/>\n" +
          "prefix skos: <http://www.w3.org/2004/02/skos/core#>\n" +
          "prefix wgs84pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n" +
          "prefix rel: <http://purl.org/vocab/relationship/>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm/>\n" +
          "prefix idiomcat: <http://idiom-projekt.de/catalogue/>\n" +
          "\n";

  /**
   * 
   */
  public FusekiCRUD() {
    this.accessor = DatasetAccessorFactory.createHTTP("http://localhost:3030/fuseki/metadata");
  }

  /**
   * @param tgURI
   * @param rdfXML
   */
  public void create(String tgURI, String rdfXML) {

    InputStream inStream = new ByteArrayInputStream(rdfXML.getBytes());
    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "RDF/XML");

    this.accessor.add(tgURI, model);
  }

  /**
   * @param tgURI
   * @param rdfXML
   */
  public void update(String tgURI, String rdfXML) {
    this.delete(tgURI);
    this.create(tgURI, rdfXML);
  }

  /**
   * @param tgURI
   */
  public void delete(String tgURI) {
    this.accessor.deleteModel(tgURI);
  }

  /**
   * 
   */
  public void print() {
    Model model = this.accessor.getModel("textgrid:2r45c");
    String graph = model.toString();
    InputStream stream = new ByteArrayInputStream(graph.getBytes(StandardCharsets.UTF_8));
    model.read(stream, null, "RDF/XML");
  }

}
