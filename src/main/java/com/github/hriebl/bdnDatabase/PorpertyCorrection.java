package com.github.hriebl.bdnDatabase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.core.Response;
import javax.xml.ws.Holder;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 *
 */
public class PorpertyCorrection {

  private static TGCrudService tgCRUD;
  public static List<String> URIListAllProductionWithProducer = new ArrayList<String>();
  public static List<String> subforms = new ArrayList<String>();
  public static List<String> shapeAssignmentsWithNoStatus = new ArrayList<String>();
  public static Map<String, String> zoteroCitations = new Hashtable<String, String>();
  public static boolean material = false;
  public static boolean technique = false;
  public static String prefixes = "prefix dct: <http://purl.org/dc/terms#>\n" +
      "              prefix dc: <http://purl.org/dc/elements/1.1/>\n" +
      "              prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
      "              prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
      "              prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
      "              prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
      "              prefix idi: <http://idiom.uni-bonn.de/terms#>\n" +
      "              prefix idiom: <http://idiom-projekt.de/schema/>\n" +
      "              prefix ecrm: <http://erlangen-crm.org/current#>\n" +
      "              prefix edm: <http://www.europeana.eu/schemas/edm#>\n" +
      "              prefix form: <http://rdd.sub.uni-goettingen.de/rdfform#>\n" +
      "              prefix crm: <http://erlangen-crm.org/current/>\n" +
      "              prefix tgforms: <http://www.tgforms.de/terms#>\n" +
      "              prefix textgrid: <http://textgridrep.de/>\n" +
      "              prefix schema: <http://schema.org/>\n" +
      "              prefix skos: <http://www.w3.org/2004/02/skos/core#>\n" +
      "              prefix wgs84pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n" +
      "              prefix rel: <http://purl.org/vocab/relationship/>\n" +
      "              prefix edm: <http://www.europeana.eu/schemas/edm/>\n" +
      "			   prefix idiomcat: <http://idiom-projekt.de/catalogue/>";
  static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  static SecureRandom rnd = new SecureRandom();

  /**
   * @param len
   * @return
   */
  static String randomString(int len) {
    StringBuilder sb = new StringBuilder(len);
    for (int i = 0; i < len; i++)
      sb.append(AB.charAt(rnd.nextInt(AB.length())));
    return sb.toString();
  }

  /**
   * @param args
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws IOException
   * @throws UpdateConflictFault
   */
  public static void main(String args[])
      throws MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, IOException,
      UpdateConflictFault {
    try {
      correctProperties();
    } catch (ObjectNotFoundFault e) {
      // TODO Auto-generated catch block
      // System.out.println("DUMDUM");
      e.printStackTrace();
    }
  }

  /**
   * @return
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws IOException
   * @throws UpdateConflictFault
   */
  public static Response correctProperties() throws ObjectNotFoundFault, MetadataParseFault,
      IoFault, ProtocolNotImplementedFault, AuthFault, IOException, UpdateConflictFault {

    String tgSID =
        "ALflhNjGotjcGcKPOYiBcz5MRduf3L9kv1CMAjw50UJkFismQ5pzY8FL6qY0liUMcriYeHh1605342771977517";
    tgCRUD = TGCrudClientUtilities.getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService");
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
    Holder<DataHandler> dataHolder = new Holder<DataHandler>();

    int counterForObjectsToProceeds = getAllArtefact().size();
    int i = 1;

    for (String artURI : getAllArtefact()) {
      try {
        tgCRUD.read(tgSID, "", artURI.replace("http://textgridrep.de/", "textgrid:"),
            metadataHolder, dataHolder);
        metadataHolder.value.getObject().getGeneric().getProvided().setNotes("ARTEFACT");
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        dataHolder.value.writeTo(bs);

        String content = new String(bs.toByteArray());

        DataSource dataSource = new ByteArrayDataSource(content.getBytes(), "UTF-8");
        DataHandler tgData = new DataHandler(dataSource);

        artURI = artURI.replace(".0", "");
        tgCRUD.update(tgSID, "", metadataHolder, tgData);
        counterForObjectsToProceeds--;
        System.out.println(counterForObjectsToProceeds + " objects left");
        i++;
      } catch (ObjectNotFoundFault f) {
        System.out.println("Not possible for" + artURI);
      }
    }

    return Response.ok().build();
  }

  /**
   * @param activityURI
   * @return
   */
  public static String getActivityTitle(String activityURI) {

    String query = prefixes + " " + "SELECT ?activity ?activityTitle\n" +
        "WHERE {GRAPH  <" + activityURI.replace("http://textgridrep.de/", "textgrid:") + "> {\n" +
        "    ?activity rdf:type idiom:Dedication.\n" +
        "    ?activity idiom:activityTitle ?activityTitle;\n" +
        "                        FILTER (regex (str(?activityTitle), \"Production\") )\n" +
        "  }\n" +
        "  }";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();

    QuerySolution soln = results.nextSolution();
    RDFNode artefactURI = soln.get("activityTitle");

    return artefactURI.toString();
  }

  /**
   * @return
   */
  public static List<String> getAllArtefact() {
    List<String> allArtefactList = new ArrayList<String>();
    String query = prefixes + " SELECT * WHERE { GRAPH ?artefactGraph {\n" +
        "    ?artefact rdf:type idiom:Artefact.\n" +
        "  }\n" +
        "}OFFSET 2300";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("artefact");
      allArtefactList.add(artefactURI.toString());
    }

    return allArtefactList;
  }

  /**
   * 
   */
  public static void getShapeAssignmentsWithNoStatus() {

    String query = prefixes + " SELECT * WHERE { GRAPH ?artefactGraph {\n" +
        "    ?artefact rdf:type idiom:Artefact.\n" +
        "    ?artefact idiom:hasTechniqueAssignment ?shapeAssignment.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?shapeAssignment,' + \">\" + ')) AS ?shapeAssignmentRef)\n"
        +
        "    GRAPH ?shapeAssignmentRef{\n" +
        "      FILTER NOT EXISTS {?shapeAssignment idiom:status ?status.}\n" +
        "    }\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("artefact");

      shapeAssignmentsWithNoStatus.add(artefactURI.toString());
    }
  }

  /**
   * @param prodURI
   * @return
   */
  public static String setSourceForMaterial(String prodURI) {

    String query = prefixes + " " +
        "SELECT ?production ?URIconcept ?zot ?bibCit ?page ?attr ?confidenceLevel \n" +
        "WHERE {GRAPH  <" + prodURI + "> {\n" +
        "   ?production crm:P126_employed ?URIconcept.\n" +
        "   OPTIONAL {?production idiom:sourceMaterial ?biblRef.\n" +
        "   BIND(IRI(CONCAT(' + \"<\" + ',?biblRef,' + \">\" + ')) AS ?biblUrl)\n" +
        "        GRAPH ?biblUrl {\n" +
        "    	  ?biblRef idiom:zoteroURI ?zot.\n" +
        "          ?biblRef dct:bibliographicCitation ?bibCit." +
        "          OPTIONAL {?biblRef schema:pagination ?page.}" +
        "          OPTIONAL {?biblRef idiom:attributionType ?attr.}" +
        "          ?biblRef idiom:hasConfidenceLevel ?confidenceLevel." +
        "        }\n" +
        "   }" +
        " }" +
        "}";

    // System.out.println(query);

    QueryExecution q2 = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results2 = q2.execSelect();

    QuerySolution soln = results2.nextSolution();
    if (soln.get("zot") != null) {
      RDFNode zoteroURI = soln.get("zot");
      RDFNode citation = soln.get("bibCit");
      RDFNode page = soln.get("page");
      RDFNode attr = soln.get("attr");
      RDFNode confLevel = soln.get("confidenceLevel");

      String pageString = "";
      String attrString = "";
      if (soln.get("page") != null) {
        pageString = "<schema:pagination>" + page.toString() + "</schema:pagination>";
      }
      if (soln.get("attr") != null) {
        attrString = "<idiom:attributionType>" + attr.toString() + "</idiom:attributionType>";
      }

      return "<dct:isReferencedBy>"
          + "<dct:isReferencedBy rdf:about=\"http://purl.org/dc/terms#isReferencedBy"
          + randomString(10) + "\">"
          + "<idiom:zoteroURI>" + zoteroURI.toString() + "</idiom:zoteroURI>"
          + "<dct:bibliographicCitation>" + citation.toString() + "</dct:bibliographicCitation>"
          + pageString
          + attrString
          + "<idiom:hasConfidenceLevel>" + confLevel.toString() + "</idiom:hasConfidenceLevel>"
          + "</dct:isReferencedBy></dct:isReferencedBy>";
    } else {
      return "";
    }
  }

  /**
   * @param prodURI
   * @return
   */
  public static String setSourceForTechnique(String prodURI) {

    String query = prefixes + " " +
        "SELECT ?production ?URIconcept ?zot ?bibCit ?page ?attr ?confidenceLevel \n" +
        "WHERE {GRAPH  <" + prodURI + "> {\n" +
        "   ?production crm:P32_used_general_technique ?URIconcept.\n" +
        "   OPTIONAL {?production idiom:sourceTechnique ?biblRef.\n" +
        "   BIND(IRI(CONCAT(' + \"<\" + ',?biblRef,' + \">\" + ')) AS ?biblUrl)\n" +
        "        GRAPH ?biblUrl {\n" +
        "    	  ?biblRef idiom:zoteroURI ?zot.\n" +
        "          ?biblRef dct:bibliographicCitation ?bibCit." +
        "          OPTIONAL {?biblRef schema:pagination ?page.}" +
        "          OPTIONAL {?biblRef idiom:attributionType ?attr.}" +
        "          ?biblRef idiom:hasConfidenceLevel ?confidenceLevel." +
        "        }\n" +
        "   }" +
        " }" +
        "}";

    QueryExecution q2 = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results2 = q2.execSelect();

    QuerySolution soln = results2.nextSolution();
    if (soln.get("zot") != null) {
      RDFNode zoteroURI = soln.get("zot");
      RDFNode citation = soln.get("bibCit");
      RDFNode page = soln.get("page");
      RDFNode attr = soln.get("attr");
      RDFNode confLevel = soln.get("confidenceLevel");

      String pageString = "";
      String attrString = "";
      if (soln.get("page") != null) {
        pageString = "<schema:pagination>" + page.toString() + "</schema:pagination>";
      }
      if (soln.get("attr") != null) {
        attrString = "<idiom:attributionType>" + attr.toString() + "</idiom:attributionType>";
      }

      return "<dct:isReferencedBy>"
          + "<dct:isReferencedBy rdf:about=\"http://purl.org/dc/terms#isReferencedBy"
          + randomString(10) + "\">"
          + "<idiom:zoteroURI>" + zoteroURI.toString() + "</idiom:zoteroURI>"
          + "<dct:bibliographicCitation>" + citation.toString() + "</dct:bibliographicCitation>"
          + pageString
          + attrString
          + "<idiom:hasConfidenceLevel>" + confLevel.toString() + "</idiom:hasConfidenceLevel>"
          + "</dct:isReferencedBy></dct:isReferencedBy>";
    } else {
      return "";
    }
  }

  /**
   * 
   */
  public static void collectAllActivitiesWithDedicationInTitle() {

    String query = prefixes + " " + "SELECT ?activity ?activityTitle\n" +
        "WHERE {GRAPH  ?subject {\n" +
        "    ?activity rdf:type crm:E7_Activity.\n" +
        "    ?activity idiom:activityTitle ?activityTitle;\n" +
        "                        FILTER (regex (str(?activityTitle), \"Dedication\") )\n" +
        "  }\n" +
        "  }";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("activity");

      URIListAllProductionWithProducer.add(artefactURI.toString());
    }
  }

  /**
   * 
   */
  public static void collectAllArtefactsWithProduction() {

    String query = prefixes + " " + "SELECT ?activity ?activityTitle\n" +
        "WHERE {GRAPH  ?subject {\n" +
        "    ?activity rdf:type idiom:Dedication.\n" +
        "    ?activity idiom:activityTitle ?activityTitle;\n" +
        "                        FILTER (regex (str(?activityTitle), \"Production\") )\n" +
        "  }\n" +
        "  }";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("activity");

      URIListAllProductionWithProducer.add(artefactURI.toString());
    }
  }

  /**
   * @param artURI
   * @return
   */
  public static String getProductionURI(String artURI) {

    System.out.println("URI: " + artURI);
    String queryForProduction =
        prefixes + " " + "SELECT DISTINCT ?refToProduction ?artefact ?material ?technique\n" +
            "WHERE {GRAPH  <" + artURI.replace("http://textgridrep.de/", "textgrid:") + "> {\n" +
            "   ?artefact rdf:type idiom:Artefact.\n" +
            "   ?artefact crm:P108i_was_produced_by ?refToProduction.\n" +
            "   GRAPH ?production {\n" +
            "      OPTIONAL {?refToProduction crm:P126_employed ?material}\n" +
            "      OPTIONAL {?refToProduction crm:P32_used_general_technique ?technique}\n" +
            "    }\n" +
            "  }\n" +
            "}";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", queryForProduction);
    ResultSet results = q.execSelect();

    QuerySolution soln = results.nextSolution();
    RDFNode productionURI = soln.get("refToProduction");

    if (soln.get("material") != null) {
      material = true;
    }
    if (soln.get("technique") != null) {
      technique = true;
    }

    String prodURI = productionURI.toString();

    return prodURI.replace("http://textgridrep.de/", "textgrid:");
  }

  /**
   * @param artURI
   * @return
   */
  public static String getProductionMaterialInfo(String artURI) {

    String query = prefixes + " " +
        "SELECT ?production ?URIconcept ?zot \n" +
        "WHERE {GRAPH  <" + artURI.replace("http://textgridrep.de/", "textgrid:") + "> {\n" +
        "   ?production crm:P126_employed ?URIconcept.\n" +
        "   OPTIONAL {?production idiom:sourceMaterial ?biblRef.\n" +
        "   BIND(IRI(CONCAT(' + \"<\" + ',?biblRef,' + \">\" + ')) AS ?biblUrl)\n" +
        "        GRAPH ?biblUrl {\n" +
        "    	  ?biblRef idiom:zoteroURI ?zot.\n" +
        "        }\n" +
        "   }" +
        " }" +
        "}";

    QueryExecution q2 = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results2 = q2.execSelect();

    QuerySolution soln2 = results2.nextSolution();
    RDFNode artefactURI = soln2.get("URIconcept");

    return artefactURI.toString();
  }

  /**
   * @param artURI
   * @return
   */
  public static String getProductionTechniqueInfo(String artURI) {

    String query = prefixes + " " +
        "SELECT ?production ?URIconcept ?zot \n" +
        "WHERE {GRAPH  <" + artURI.replace("http://textgridrep.de/", "textgrid:") + "> {\n" +
        "   OPTIONAL {?production crm:P32_used_general_technique ?URIconcept.}\n" +
        " }" +
        "}";

    if (technique) {
      QueryExecution q = QueryExecutionFactory
          .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
      ResultSet results = q.execSelect();

      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("URIconcept");

      return artefactURI.toString();
    } else {
      return "";
    }
  }

}
