package com.github.hriebl.bdnDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Holder;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 *
 */
@Path("/")
public class Server {

  private FusekiCRUD fusekiCRUD;
  private TGCrudService tgCRUD;

  /**
   * @throws MalformedURLException
   */
  public Server() throws MalformedURLException {
    this.tgCRUD =
        TGCrudClientUtilities.getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService?wsdl");
  }

  /**
   * @param tgSID
   * @param tgPID
   * @param title
   * @return
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws UnsupportedEncodingException
   */
  @POST
  @Path("/create")
  @Produces("application/ld+json")
  public String create(@FormParam("tgSID") String tgSID, @FormParam("tgPID") String tgPID,
      @FormParam("title") String title) throws AuthFault, IoFault, MetadataParseFault,
      ObjectNotFoundFault, UnsupportedEncodingException {

    System.out.println("I WANT TO CREATE");

    ProvidedType providedMeta = new ProvidedType();
    providedMeta.setFormat("text/tg.inputform+rdf+xml");
    providedMeta.getTitle().add(title);

    GenericType genericMeta = new GenericType();
    genericMeta.setProvided(providedMeta);

    ObjectType objectMeta = new ObjectType();
    objectMeta.setGeneric(genericMeta);

    MetadataContainerType metaContainer = new MetadataContainerType();
    metaContainer.setObject(objectMeta);

    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
    tgMeta.value = metaContainer;

    String string = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"/>";
    DataSource dataSource = new ByteArrayDataSource(string.getBytes("UTF-8"), "UTF-8");
    DataHandler tgData = new DataHandler(dataSource);

    this.tgCRUD.create(tgSID, "", null, false, tgPID, tgMeta, tgData);

    genericMeta = tgMeta.value.getObject().getGeneric();
    String uri = genericMeta.getGenerated().getTextgridUri().getValue();
    uri = uri.replaceFirst("\\.[0-9]*$", "");

    return "{\"@id\": \"" + uri + "\"}";
  }

  /**
   * @param tgURI
   * @param tgSID
   * @return
   * @throws AuthFault
   * @throws IOException
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   */
  @GET
  @Path("/read/{tgURI}")
  @Produces("text/turtle")
  public String read(@PathParam("tgURI") String tgURI, @QueryParam("tgSID") String tgSID)
      throws AuthFault, IOException, IoFault, MetadataParseFault, ObjectNotFoundFault,
      ProtocolNotImplementedFault {

    System.out.println("I WANT TO READ");

    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
    Holder<DataHandler> tgData = new Holder<DataHandler>();

    this.tgCRUD.read(tgSID, "", tgURI, tgMeta, tgData);

    InputStream inStream = tgData.value.getInputStream();

    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "RDF/XML");
    OutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, "TURTLE");

    System.out.println("I WANT TO READ: " + tgURI);

    return outStream.toString();
  }

  /**
   * @param tgURI
   * @param tgSID
   * @param jsonLD
   * @return
   * @throws AuthFault
   * @throws IOException
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws UpdateConflictFault
   */
  @POST
  @Path("/update/{tgURI}")
  @Consumes("application/ld+json")
  public Response write(@PathParam("tgURI") String tgURI, @QueryParam("tgSID") String tgSID,
      String jsonLD) throws AuthFault, IOException, IoFault, MetadataParseFault,
      ObjectNotFoundFault, UpdateConflictFault {

    System.out.println("I WANT TO UPDATE: " + tgURI);

    MetadataContainerType metaContainer = this.tgCRUD.readMetadata(tgSID, "", tgURI);
    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
    tgMeta.value = metaContainer;

    InputStream inStream = new ByteArrayInputStream(jsonLD.getBytes());

    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "JSON-LD");

    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, "RDF/XML");
    byte[] byteArray = outStream.toByteArray();

    String content = new String(outStream.toByteArray());
    if (content.contains("idiom:Artefact")) {
      tgMeta.value.getObject().getGeneric().getProvided().setNotes("ARTEFACT");
    }
    DataSource dataSource = new ByteArrayDataSource(byteArray, "UTF-8");
    DataHandler tgData = new DataHandler(dataSource);

    this.tgCRUD.update(tgSID, "", tgMeta, tgData);

    String tgPID = tgMeta.value.getObject().getGeneric().getGenerated().getProject().getId();

    if (tgPID.equals("TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318")) {
      String rdfXML = new String(byteArray, "UTF-8");
      this.fusekiCRUD.update(tgURI, rdfXML);
    }

    return Response.ok().build();
  }

  /**
   * @param tgURI
   * @param tgSID
   * @return
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws RelationsExistFault
   */
  @DELETE
  @Path("/delete/{tgURI}")
  public Response delete(@PathParam("tgURI") String tgURI, @QueryParam("tgSID") String tgSID)
      throws AuthFault, IoFault, ObjectNotFoundFault, RelationsExistFault {

    this.tgCRUD.delete(tgSID, "", tgURI);
    this.fusekiCRUD.delete(tgURI);

    return Response.ok().build();
  }

  /**
   * @param query
   * @return
   * @throws IOException
   * @throws MalformedURLException
   * @throws TransformerException
   */
  @GET
  @Path("/swb")
  @Produces("application/xml")
  public String swb(@QueryParam("query") String query)
      throws IOException, MalformedURLException, TransformerException {

    TransformerFactory factory = TransformerFactory.newInstance();

    query = query.replaceAll(" ", "+");

    URL xmlURL = new URL("http://swb.bsz-bw.de/sru/DB=2.1/username=/" +
        "password=/?maximumRecords=999&operation=searchRetrieve&" +
        "recordSchema=marc21&query=pica.all%3D%22" + query + "%22");

    String xmlString = IOUtils.toString(xmlURL.openStream());
    xmlString = xmlString.replaceFirst("(srw:searchRetrieveResponse)",
        "$1 xmlns=\"http://www.loc.gov/MARC21/slim\"");

    InputStream xmlStream = new ByteArrayInputStream(xmlString.getBytes());
    Source xmlSource = new StreamSource(xmlStream);

    URL xslURL = new URL("http://localhost:9090/marc2mods.xsl");
    Source xslSource = new StreamSource(xslURL.openStream());

    Transformer transformer = factory.newTransformer(xslSource);

    OutputStream stream = new ByteArrayOutputStream();
    StreamResult result = new StreamResult(stream);

    transformer.transform(xmlSource, result);

    return stream.toString();
  }

}
